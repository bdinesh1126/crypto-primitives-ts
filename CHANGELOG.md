# Changelog

## Release 1.3.3

Release 1.3.3 is a minor maintenance patch containing the following changes:

* Updated dependencies and third-party libraries.

## Release 1.3.2

Release 1.3.2 is a minor maintenance patch containing the following changes:

* Updated dependencies and third-party libraries.

## Release 1.3.1

Release 1.3.1 includes some feedback from the Federal Chancellery's mandated experts and other experts of the community.
We want to thank the experts for their high-quality, constructive remarks:

* Thomas Edmund Haines (Australian National University), Olivier Pereira (Université catholique Louvain), Vanessa Teague (Thinking Cybersecurity)
* Aleksander Essex (Western University Canada)
* Rolf Haenni, Reto Koenig, Philipp Locher, Eric Dubuis (Bern University of Applied Sciences)

The following functionalities and improvements are included in release 1.3.1:

* Adapted algorithms RecurisveHashToZq and HashAndSquare (feedback from Thomas Haines, Olivier Pereira, and Vanessa Teague).
* Optimized the GenExponentiationProof algorithm by omitting the costly input check on the input elements. Included a justification.
* VerifyExponentiation - merged lines to be consistent with the proof generation (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Updated dependencies and third-party libraries.

## Release 1.3.0

Release 1.3.0 includes some feedback from the Federal Chancellery's mandated experts (see above) and other experts of the community.

The following functionalities and improvements are included in release 1.3.0:

* Added a validation in the StringToByteArray method that the input is a valid UTF-8 string (feedback from Vanessa Teague, Olivier Pereira, and Thomas Haines and reported in GitLab Issue [46 / #YWH-PGM232-122](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/46)).
* Improved the performance of the verification of the zero-knowledge proofs.
* Centralized the Argon2id parameters.
* Implemented the Base16 encode and decode methods.
* Updated dependencies and third-party libraries.

---

## Release 1.2.1

Release 1.2.1 includes some feedback from the Federal Chancellery's mandated experts (see above)

The following functionalities and improvements are included in release 1.2.1:

* Improved the RecursiveHashToZq algorithm to ensure that it is collision resistant.
* Updated dependencies and third-party libraries.

---

## Release 1.2

Release 1.2 includes some feedback from the Federal Chancellery's mandated experts (see above)

The following functionalities and improvements are included in release 1.2:

* Improved the secure random generator when invoked inside a WebWorker (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the alignment of the GenRandomInteger algorithm and implemented the byteLength method (feedback from Rolf Haenni, Reto Koenig, Philipp
  Locher, and Eric Dubuis).
* Simplified the VerifyPlaintext algorithm (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Introduced a divide method for GqElements (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Implemented the Schnorr proofs.
* Updated dependencies, notably the Verificatum typescript library.

---

## Release 1.1

Release 1.1 includes some feedback from the Federal Chancellery's mandated experts (see above)

The following functionalities and improvements are included in release 1.1:

* Implemented the Base16Encode and Base16Decode wrapper methods (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Improved the immutability of objects (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Implemented the prime-gq-element objects.
* Improved the alignment of the method GenRandomInteger (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).
* Removed the probabilistic primality tests (feedback from Rolf Haenni, Reto Koenig, Philipp Locher, and Eric Dubuis).

---

## Release 1.0

Release 1.0 includes the following improvements:

* Split the algorithm Argon2id into two separate algorithm GenArgon2id and GetArgon2id.
* Improved the RecursiveHash's collision-resistance for nested objects (refers to #YWH-PGM2323-69 mentionned
  in [GitLab issue #37](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/37)).
* Ensured the injective encoding of the associated data in the GenCiphertextSymmetric method (refers to #YWH-PGM2323-70 mentionned
  in [GitLab issue #37](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/issues/37)).
* Switched the underlying cryptographic library to
  the [Verificatum TypeScript Big Integer library](https://github.com/verificatum/verificatum-vts-ba), implemented by Douglas Wikström.

---

## Release 0.15

Release 0.15 includes the following improvements:

* Implemented the methods GenCiphertextSymmetric and GetMessageSymmetric
* Renamed the methods multiply and exponentiate to GetCiphertextProduct and GetCiphertextExponentiation

---

## Release 0.14

Release 0.14 includes the following improvement:

* Fixed a minor mistake in the IntegerToString implementation.
