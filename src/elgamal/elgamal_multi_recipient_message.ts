/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ElGamalMultiRecipientObject} from "./elgamal_multi_recipient_object";
import {GqElement} from "../math/gq_element";
import {GqGroup} from "../math/gq_group";
import {GroupVector} from "../group_vector";
import {checkArgument} from "../validation/preconditions";
import {Hashable} from "../hashing/hashable";

export class ElGamalMultiRecipientMessage implements ElGamalMultiRecipientObject<GqElement, GqGroup> {

	private readonly messageElementsInternal: GroupVector<GqElement, GqGroup>;

	constructor(messageElements: GqElement[]) {
		this.messageElementsInternal = GroupVector.from(messageElements);
		checkArgument(this.messageElementsInternal.elementSize > 0, "An ElGamal message must not be empty.")
	}

	get group(): GqGroup {
		return this.messageElementsInternal.group;
	}

	get size(): number {
		return this.messageElementsInternal.elements.length;
	}

	get(i: number): GqElement {
		return this.messageElementsInternal.get(i);
	}

	stream(): GqElement[] {
		return this.messageElementsInternal.elements;
	}

	toHashableForm(): Hashable {
		return undefined;
	}

	equals(o: ElGamalMultiRecipientMessage): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}
		return this.messageElementsInternal.equals(o.messageElementsInternal);
	}


}