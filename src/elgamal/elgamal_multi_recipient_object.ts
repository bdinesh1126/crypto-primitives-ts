/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupElement} from "../math/group_element";
import {MathematicalGroup} from "../math/mathematical_group";
import {GroupVectorElement} from "../group_vector_element";

/**
 * Defines a common API for El Gamal multi-recipient objects.
 */

export interface ElGamalMultiRecipientObject<E extends GroupElement<G>, G extends MathematicalGroup<G>> extends GroupVectorElement<G> {

	/**
	 * @param i the index of the element to return
	 * @return the ith element of this actor.
	 */
	get(i: number): E;

	/**
	 * @return an array of this object's elements.
	 */
	stream(): E[];
}