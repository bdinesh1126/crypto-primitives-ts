/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ElGamalMultiRecipientObject} from "./elgamal_multi_recipient_object";
import {GqElement} from "../math/gq_element";
import {GqGroup} from "../math/gq_group";
import {GroupVector} from "../group_vector";
import {ElGamalMultiRecipientMessage} from "./elgamal_multi_recipient_message";
import {ZqElement} from "../math/zq_element";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ElGamalMultiRecipientPublicKey} from "./elgamal_multi_recipient_public_key";
import {Hashable} from "../hashing/hashable";
import {ImmutableArray} from "../immutable_array";

/**
 * An ElGamal multi-recipient ciphertext composed of a gamma and a list of phi (γ, 𝜙₀,..., 𝜙ₗ₋₁). The gamma is the left-hand side of a standard
 * ElGamal encryption. Each phi is the encryption of a different message, using a different public key element and the same randomness.
 * <p>
 * An ElGamal multi-recipient ciphertext cannot be empty. It contains always a γ and at least one 𝜙.
 *
 * <p>Instances of this class are immutable.
 */
export class ElGamalMultiRecipientCiphertext implements ElGamalMultiRecipientObject<GqElement, GqGroup> {

	private readonly gammaInternal: GqElement;
	private readonly phisInternal: GroupVector<GqElement, GqGroup>;
	private readonly groupInternal: GqGroup;

	// Private constructor without input validation. Used only to internally construct new ciphertext whose elements have already been validated.
	private constructor(gamma: GqElement, phis: GroupVector<GqElement, GqGroup>) {
		this.gammaInternal = gamma;
		this.phisInternal = phis;
		this.groupInternal = gamma.group;
	}

	get gamma(): GqElement {
		return this.gammaInternal;
	}

	get phis(): GroupVector<GqElement, GqGroup> {
		return this.phisInternal;
	}

	get group(): GqGroup {
		return this.groupInternal;
	}

	/**
	 * @return the number of phis in the ciphertext.
	 */
	get size(): number {
		return this.phisInternal.elements.length;
	}

	/**
	 * Encrypts a message with the given public key and provided randomness.
	 * <p>
	 * The <code>message</code>, <code>exponent</code> and <code>publicKey</code> parameters must comply with the following:
	 * <ul>
	 *     <li>the message size must be at most the public key size.</li>
	 *     <li>the message and the public key groups must be the same.</li>
	 *     <li>the message and the exponent must belong to groups of same order.</li>
	 * </ul>
	 *
	 * @param message   m, the plaintext message. Must be non null and not empty.
	 * @param exponent  r, a random exponent. Must be non null.
	 * @param publicKey pk, the public key to use to encrypt the message. Must be non null.
	 * @return A ciphertext containing the encrypted message.
	 */
	public static getCiphertext(message: ElGamalMultiRecipientMessage, exponent: ZqElement,
								publicKey: ElGamalMultiRecipientPublicKey): ElGamalMultiRecipientCiphertext {

		checkNotNull(message);
		checkNotNull(exponent);
		checkNotNull(publicKey);
		checkArgument(message.group.hasSameOrderAs(exponent.group), "Exponent and message groups must be of the same order.");
		checkArgument(message.group.equals(publicKey.group), "Message and public key must belong to the same group.");
		checkArgument(0 < message.size, "The message must contain at least one element.");
		checkArgument(message.size <= publicKey.size, "There cannot be more message elements than public key elements.");

		const m: ElGamalMultiRecipientMessage = message;
		const r: ZqElement = exponent;
		const pk: ElGamalMultiRecipientPublicKey = publicKey;

		const l: number = m.size;
		const g: GqElement = pk.group.generator;

		// Algorithm.
		const gamma: GqElement = g.exponentiate(r);

		const phis: GqElement[] = [];
		for (let i = 0; i < l; i++) {
			phis[i] = pk.get(i).exponentiate(r).multiply(m.get(i));
		}

		return new ElGamalMultiRecipientCiphertext(gamma, GroupVector.from(phis));
	}

	/**
	 * Creates a <code>ElGamalMultiRecipientCiphertext</code> using the specified gamma and phi values.
	 * @param gamma The gamma (i.e. first) element of the ciphertext. <code>gamma</code> must be a valid GqElement different from the GqGroup generator.
	 * @param phis  The phi elements of the ciphertext, which must satisfy the following:
	 *              <ul>
	 *              <li>The list must be non-null.</li>
	 *              <li>The list must not be empty.</li>
	 *              <li>The list must not contain any null.</li>
	 *              <li>All elements must be from the same Gq group as gamma.</li>
	 *              </ul>
	 * @return A new ElGamalMultiRecipientCiphertext with the specified gamma and phis
	 */
	public static create(gamma: GqElement, phis: GqElement[]): ElGamalMultiRecipientCiphertext {
		checkNotNull(gamma);

		const phisVector: GroupVector<GqElement, GqGroup> = GroupVector.from(phis);

		checkArgument(phisVector.elementSize > 0, "An ElGamalMultiRecipientCiphertext phis must be non empty.");
		checkArgument(gamma.group.equals(phisVector.group), "Gamma and phis must belong to the same GqGroup.");

		return new ElGamalMultiRecipientCiphertext(gamma, phisVector);
	}

	/**
	 * Implements the specification GetCiphertextExponentiation algorithm. It exponentiates each element of the multi-recipient ciphertext by an exponent a.
	 * <p>
	 * The {@code exponent} parameter must comply with the following:
	 * <ul>
	 *     <li>the ciphertext to exponentiate and the exponent must belong to groups of same order.</li>
	 * </ul>
	 *
	 * @param exponent a, a {@code ZqElement}. Must be non null.
	 * @return a ciphertext whose gamma and phis values are exponentiated with {@code exponent}.
	 * @throws NullPointerError if the exponent is null
	 * @throws IllegalArgumentError if the exponent's group does not have the same order as this ciphertext's group
	 */
	public getCiphertextExponentiation(exponent: ZqElement): ElGamalMultiRecipientCiphertext {
		checkNotNull(exponent);
		checkArgument(this.group.hasSameOrderAs(exponent.group));
		const a: ZqElement = exponent;
		const l = this.phis.size;

		const gamma: GqElement = this.gamma.exponentiate(a);
		const phi: GqElement[] = [];
		phi.length = l;
		for (let i = 0; i < l; i++) {
			phi[i] = this.phis.get(i).exponentiate(a);
		}

		return new ElGamalMultiRecipientCiphertext(gamma, GroupVector.from(phi));
	}

	/**
	 * @return the ith phi element.
	 */
	get(i: number): GqElement {
		return this.phisInternal.get(i);
	}

	stream(): GqElement[] {
		return [this.gammaInternal, ...this.phisInternal.elements];
	}

	toHashableForm(): Hashable {
		return ImmutableArray.from(this.stream().map(el => el.toHashableForm()));
	}

	equals(o: ElGamalMultiRecipientCiphertext): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}

		return this.gammaInternal.equals(o.gammaInternal)
			&& this.phisInternal.group.equals(o.phisInternal.group)
			&& this.phisInternal.elements.every((el, index) => el.equals(o.phisInternal.get(index)));
	}


}
