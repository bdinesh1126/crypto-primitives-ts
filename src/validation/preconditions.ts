/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {IllegalArgumentError} from "../error/illegal_argument_error";
import {NullPointerError} from "../error/null_pointer_error";


export function checkArgument(b: boolean, message?: string): void {
	if (!b) {
		throw new IllegalArgumentError(message);
	}
}

export function checkNotNull<T>(reference: T, message?: string): T {
	if (reference == null) {
		throw new NullPointerError(message);
	} else {
		return reference;
	}
}