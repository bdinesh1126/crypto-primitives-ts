/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupVectorElement} from "./group_vector_element";
import {MathematicalGroup} from "./math/mathematical_group";
import {checkArgument, checkNotNull} from "./validation/preconditions";
import {IllegalStateError} from "./error/illegal_state_error";
import {ImmutableArray} from "./immutable_array";
import {Hashable} from "./hashing/hashable";

/**
 * Represents a vector of {@link GroupElement} belonging to the same {@link MathematicalGroup} and having the same size.
 * <p>
 * This is effectively a decorator for the ImmutableList class.
 *
 * @param <E> the type of elements this list contains.
 * @param <G> the group type the elements of the list belong to.
 */
export class GroupVector<E extends GroupVectorElement<G>, G extends MathematicalGroup<G>> implements GroupVectorElement<G> {

	private readonly elementsInternal: E[];
	private groupInternal: G;
	private elementSizeInternal: number;

	private constructor(elements: E[]) {
		this.elementsInternal = elements;
		this.groupInternal = elements.length === 0 ? null : elements[0].group;
		this.elementSizeInternal = elements.length === 0 ? 0 : elements[0].size;
	}

	/**
	 * Returns a GroupVector of {@code elements}.
	 *
	 * @param elements the list of elements contained by this vector, which must respect the following:
	 *                 <ul>
	 *                 <li>the list must be non-null</li>
	 *                 <li>the list must not contain any nulls</li>
	 *                 <li>all elements must be from the same {@link MathematicalGroup} </li>
	 *                 <li>all elements must be of the same size</li>
	 *                 </ul>
	 */
	public static from<E extends GroupVectorElement<G>, G extends MathematicalGroup<G>>(elements: E[]): GroupVector<E, G> {
		//Check null values
		checkNotNull(elements);
		checkArgument(elements.every((e) => e != null), "Elements must not contain nulls");

		//Immutable copy
		const elementsCopy: E[] = [...elements];

		//Check same group
		checkArgument(elementsCopy.every(e => elementsCopy[0].group.equals(e.group)), "All elements must belong to the same group.");

		//Check same size
		checkArgument(elementsCopy.every(e => elementsCopy[0].size === e.size), "All vector elements must be the same size.");

		return new GroupVector(elementsCopy);
	}

	/**
	 * Returns a GroupVector of {@code elements}. The elements must comply with the GroupVector constraints.
	 *
	 * @param elements The elements to be contained in this vector. May be empty.
	 * @param <E>      The type of the elements.
	 * @param <G>      The group of the elements.
	 * @return A GroupVector containing {@code elements}.
	 */
	public static of<E extends GroupVectorElement<G>, G extends MathematicalGroup<G>>(...elements: E[]): GroupVector<E, G> {
		checkArgument(elements.every(e => e != null), "Elements must not contain nulls");

		return GroupVector.from(elements);
	}

	public get(index: number): E {
		return this.elementsInternal[index];
	}

	/**
	 * @return the group all elements belong to.
	 * @throws IllegalStateException if the vector is empty.
	 */
	get group(): G {
		if (this.elementsInternal && this.elementsInternal.length === 0) {
			throw new IllegalStateError("An empty GroupVector does not have a group.");
		} else {
			return this.groupInternal;
		}
	}

	get size(): number {
		return this.elementsInternal.length;
	}

	get elements(): E[] {
    return [...this.elementsInternal];
	}

	/**
	 * @return the size of elements. 0 if the vector is empty.
	 */
	get elementSize(): number {
		return this.elementSizeInternal;
	}

	public toHashableForm(): ImmutableArray<Hashable> {
		return ImmutableArray.from(this.elementsInternal.map(el => el.toHashableForm()));
	}

	public equals(o: GroupVector<E, G>): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}
		return this.elements.every((el, i) => el.equals(o.elementsInternal[i]));
	}

}
