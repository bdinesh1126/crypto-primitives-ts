/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableBigInteger} from "../immutable_big_integer";

export class BigIntegerOperations {

	private static MODULUS_CHECK_MESSAGE: string = "The modulus must be greater than 1";

	/**
	 * Multiplies two {@link BigInteger}s and take the modulus.
	 *
	 * @param n1      the multiplier
	 * @param n2      the multiplicand
	 * @param modulus the modulus &gt; 1
	 * @return the product n1 &times; n2 mod modulus
	 */
	static modMultiply(n1: ImmutableBigInteger, n2: ImmutableBigInteger, modulus: ImmutableBigInteger): ImmutableBigInteger {
		checkNotNull(n1);
		checkNotNull(n2);
		checkNotNull(modulus);
		checkArgument(modulus.compareTo(ImmutableBigInteger.ONE) > 0, BigIntegerOperations.MODULUS_CHECK_MESSAGE);

		return n1.multiply(n2).mod(modulus);
	}

	/**
	 * Exponentiates an {@link ImmutableBigInteger} by another and take the modulus. If the exponent is negative, base and modulus must be relatively prime.
	 *
	 * @param base     the base
	 * @param exponent the exponent
	 * @param modulus  the modulus &gt; 1
	 * @return the power base<sup>exponent</sup> mod modulus
	 */
	static modExponentiate(base: ImmutableBigInteger, exponent: ImmutableBigInteger, modulus: ImmutableBigInteger): ImmutableBigInteger {
		checkNotNull(base);
		checkNotNull(exponent);
		checkNotNull(modulus);
		checkArgument(exponent.compareTo(ImmutableBigInteger.ZERO) >= 0 || base.gcd(modulus).equals(ImmutableBigInteger.ONE),
			"When the exponent is negative, base and modulus must be relatively prime");
		checkArgument(modulus.compareTo(ImmutableBigInteger.ONE) > 0, BigIntegerOperations.MODULUS_CHECK_MESSAGE);
		checkArgument(modulus.testBit(0), "The modulus must be odd");

		return base.modPow(exponent, modulus);
	}

	/**
	 * Exponentiates the elements of a list of {@link BigInteger}s by the elements of a second list and multiply the resulting terms. If an exponent
	 * is negative, then the corresponding base must be relatively prime to the modulus. This operation needs both lists to be of equal size.
	 *
	 * @param bases     the list of base values
	 * @param exponents the list of exponent values
	 * @param modulus   the modulus &gt; 1
	 * @return the product of the powers b[0]^e[0] * b[1]^e[1] * ... * b[n-1]^e[n-1] mod modulus
	 */
	public static multiModExp(bases: ImmutableBigInteger[], exponents: ImmutableBigInteger[], modulus: ImmutableBigInteger): ImmutableBigInteger {
		checkNotNull(bases);
		checkArgument(bases.every((e) => e != null), "Elements must not contain nulls");
		const basesCopy: ImmutableBigInteger[] = [...bases];
		checkArgument(basesCopy.length > 0, "Bases must be non empty.");

		checkNotNull(exponents);
		checkArgument(exponents.every((e) => e != null), "Elements must not contain nulls");
		checkArgument(exponents.every(
			(e, index) => e.compareTo(ImmutableBigInteger.ZERO) >= 0 || bases[index].gcd(modulus).equals(ImmutableBigInteger.ONE)),
			"When the exponent is negative, base and modulus must be relatively prime"
		);
		const exponentsCopy: ImmutableBigInteger[] = [...exponents];

		// The next check assures also that exponentsCopy is not empty
		checkArgument(basesCopy.length === exponentsCopy.length, "Bases and exponents must have the same size");
		checkNotNull(modulus);
		checkArgument(modulus.compareTo(ImmutableBigInteger.ONE) > 0, BigIntegerOperations.MODULUS_CHECK_MESSAGE);

		return ImmutableBigInteger.modPowProd(basesCopy, exponentsCopy, modulus);
	}

	/**
	 * Inverts an element with respect to a modulus.
	 *
	 * @param n       the number to be inverted
	 * @param modulus the modulus &gt; 1
	 * @return n<sup>-1</sup> mod modulus
	 */
	public static modInvert(n: ImmutableBigInteger, modulus: ImmutableBigInteger): ImmutableBigInteger {
		checkNotNull(n);
		checkNotNull(modulus);
		checkArgument(modulus.compareTo(ImmutableBigInteger.ONE) > 0, BigIntegerOperations.MODULUS_CHECK_MESSAGE);
		checkArgument(n.gcd(modulus).equals(ImmutableBigInteger.ONE), "The number to be inverted must be relatively prime to the modulus.")

		return n.modInverse(modulus);
	}
}