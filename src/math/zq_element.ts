/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupElement} from "./group_element";
import {ZqGroup} from "./zq_group";
import {BigIntegerOperations} from "./big_integer_operations";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableBigInteger} from "../immutable_big_integer";


/**
 * Element of the group of integers modulo q.
 *
 * <p> Instances of this class are immutable.</p>
 */
export class ZqElement extends GroupElement<ZqGroup> {

	// Private constructor without input validation. Used only for operations that provide a mathematical guarantee that the element is within the
	// group (such as multiplying two elements of the same group).
	private constructor(value: ImmutableBigInteger, group: ZqGroup) {
		super(value, group);
	}

	/**
	 * Creates a new ZqElement.
	 *
	 * @param value the value of the element. Must not be null and must be an element of the group.
	 * @param group the {@link ZqGroup} to which this element belongs.
	 * @return a new ZqElement with the specified value in the given group.
	 */
	public static create(value: ImmutableBigInteger | number, group: ZqGroup): ZqElement {
		checkNotNull(value);
		checkNotNull(group);

		let val: ImmutableBigInteger;
		if (typeof value === "number") {
			val = ImmutableBigInteger.fromNumber(value);
		} else {
			val = value;
		}
		checkArgument(group.isGroupMember(val), `Cannot create a GroupElement with value ${val} as it is not an element of group ${group}`);

		return new ZqElement(val, group);
	}

	/**
	 * Returns a <code>ZqElement</code> whose value is <code>(this + exponent) mod q</code>.
	 *
	 * @param other the other ZqElement. It must be non null and belong to the same group.
	 * @return <code>(this + exponent) mod q</code>.
	 */
	public add(other: ZqElement): ZqElement {
		checkNotNull(other);
		checkArgument(this.group.equals(other.group));

		const result: ImmutableBigInteger = this.value.add(other.value).mod(this.group.q);
		return new ZqElement(result, this.group);
	}

	/**
	 * Returns an <code>Exponent</code> whose value is <code>(this - exponent) mod q</code>.
	 *
	 * @param other the other element to be subtracted from this. It must be non null and belong to the same group.
	 * @return <code> (this - other) mod q</code>.
	 */
	public subtract(other: ZqElement): ZqElement {
		checkNotNull(other);
		checkArgument(this.group.equals(other.group));

		const result: ImmutableBigInteger = this.value.subtract(other.value).mod(this.group.q);
		return new ZqElement(result, this.group);
	}

	/**
	 * Returns a <code>ZqElement</code> whose value is <code>(this * other) mod q</code>.
	 *
	 * @param other the other to be multiplied. It must be non null and belong to the same group.
	 * @return <code>(this * other) mod q</code>.
	 */
	public multiply(other: ZqElement): ZqElement {
		const resultValue: ImmutableBigInteger = BigIntegerOperations.modMultiply(this.value, other.value, this.group.q);
		return new ZqElement(resultValue, this.group);
	}

	/**
	 * Returns a <code>ZqElement</code> whose value is <code>(this ^ exponent) mod q</code>.
	 *
	 * @param exponent the exponent. It must be non null and non negative.
	 * @return <code>(this ^ exponent) mod q</code>.
	 */
	public exponentiate(exponent: ImmutableBigInteger): ZqElement {
		checkNotNull(exponent);
		checkArgument(exponent.compareTo(ImmutableBigInteger.ZERO) >= 0, "The exponent must be positive.");

		const result: ImmutableBigInteger = BigIntegerOperations.modExponentiate(this.value, exponent, this.group.q);
		return new ZqElement(result, this.group);
	}

	/**
	 * Returns an <code>ZqElement</code> whose value is <code>(-this) mod q</code>.
	 *
	 * @return <code>(-this) mod q</code>
	 */
	public negate(): ZqElement {
		return new ZqElement(this.value.negate().mod(this.group.q), this.group);
	}
}
