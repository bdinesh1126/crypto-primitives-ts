/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "./gq_group";
import {BigIntegerOperations} from "./big_integer_operations";
import {ZqElement} from "./zq_element";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {MultiplicativeGroupElement} from "./multiplicative_group_element";


/**
 * Defines a Gq group element, ie elements of the quadratic residue group of order q and mod p.
 *
 * <p>Instances of this class are immutable.
 */
export class GqElement extends MultiplicativeGroupElement {

	// Private constructor without input validation. Used only for operations that provide a mathematical guarantee that the element is within the
	// group (such as multiplying two elements of the same group).
	private constructor(value: ImmutableBigInteger, group: GqGroup) {
		super(value, group);
	}

	/**
	 * Creates a <code>GqElement</code>. The specified value should be an element of the group.
	 *
	 * @param value the value of the element. Must not be null and must be an element of the group.
	 * @param group the {@link GqGroup} to which this element belongs.
	 * @return a new GqElement with the specified value in the given group
	 */
	public static fromValue(value: ImmutableBigInteger, group: GqGroup) {
		checkNotNull(value);
		checkNotNull(group);
		checkArgument(
			group.isGroupMember(value),
			`Cannot create a GroupElement with value ${value} as it is not an element of group ${group}`
		);

		return new GqElement(value, group);
	}

	/**
	 * Creates a <code>GqElement</code> from an {@link ImmutableBigInteger} by squaring it modulo p.
	 *
	 * @param element the {@link ImmutableBigInteger} to be squared. Must be non-null.
	 * @param group the {@link GqGroup} in which to get the new GqElement. Must be non-null.
	 * @return the squared element modulo p.
	 */
	public static fromSquareRoot(element: ImmutableBigInteger, group: GqGroup) {
		checkNotNull(element);
		checkNotNull(group);

		checkArgument(element.compareTo(ImmutableBigInteger.ZERO) > 0, "The element must be strictly greater than 0");
		checkArgument(element.compareTo(group.q) < 0, "The element must be smaller than the group's order");

		const y: ImmutableBigInteger = BigIntegerOperations.modExponentiate(element, ImmutableBigInteger.fromNumber(2), group.p);
		return new GqElement(y, group);
	}


	/**
	 * Returns a <code>GqElement</code> whose value is <code>(this * element)</code>.
	 *
	 * @param other The element to be multiplied by this. It must be from the same group and non null.
	 * @return (this * element).
	 */
	public multiply(other: MultiplicativeGroupElement): GqElement {
		checkNotNull(other);
		checkArgument(this.group.equals(other.group));

		const resultValue: ImmutableBigInteger = BigIntegerOperations.modMultiply(this.value, other.value, this.group.p);
		return new GqElement(resultValue, this.group);
	}

	/**
	 * Returns a <code>GqElement</code> whose value is (this<sup>exponent</sup>).
	 *
	 * @param exponent the exponent to which this <code>SameGroupElement</code> is to be raised. It must be a member of a group of the same order and be
	 *                 non null.
	 * @return this<sup>exponent</sup>.
	 */
	public exponentiate(exponent: ZqElement): GqElement {
		checkNotNull(exponent);
		checkArgument(this.isOfSameOrderGroup(exponent));

		const valueExponentiated: ImmutableBigInteger = BigIntegerOperations.modExponentiate(this.value, exponent.value, this.group.p);
		return new GqElement(valueExponentiated, this.group);
	}

	private isOfSameOrderGroup(exponent: ZqElement): boolean {
		return this.group.hasSameOrderAs(exponent.group);
	}

	public invert(): GqElement {
		const invertedValue: ImmutableBigInteger = BigIntegerOperations.modInvert(this.value, this.group.p);
		return new GqElement(invertedValue, this.group);
	}

	public divide(divisor: GqElement): GqElement {
		checkNotNull(divisor);
		checkArgument(this.group.equals(divisor.group));

		return this.multiply(divisor.invert());
	}

}
