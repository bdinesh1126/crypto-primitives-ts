/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {MathematicalGroup} from "./mathematical_group";
import {ZqElement} from "./zq_element";
import {GqGroup} from "./gq_group";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {ImmutableBigInteger} from "../immutable_big_integer";

/**
 * Group of integers modulo q.
 *
 * <p> Instances of this class are immutable.</p>
 */
export class ZqGroup implements MathematicalGroup<ZqGroup> {
	private readonly qInternal: ImmutableBigInteger;
	private readonly identityInternal: ZqElement;

	public constructor(q: ImmutableBigInteger) {
		checkNotNull(q);
		checkArgument(q.compareTo(ImmutableBigInteger.fromNumber(2)) >= 0);

		this.qInternal = q;
		this.identityInternal = ZqElement.create(ImmutableBigInteger.ZERO, this);
	}

	/**
	 * Creates a ZqGroup with the same order as the given {@link GqGroup};
	 */
	public static sameOrderAs(gqGroup: GqGroup): ZqGroup {
		checkNotNull(gqGroup);
		return new ZqGroup(gqGroup.q);
	}

	get q(): ImmutableBigInteger {
		return this.qInternal;
	}

	get identity(): ZqElement {
		return this.identityInternal;
	}

	public isGroupMember(value: ImmutableBigInteger): boolean {
		return value != null && value.compareTo(ImmutableBigInteger.ZERO) >= 0 && value.compareTo(this.qInternal) < 0;
	}

	public hasSameOrderAs(other: MathematicalGroup<any>): boolean {
		return this.q.equals(other.q);
	}

	public equals(o: ZqGroup): boolean {
		if (this === o) {
			return true;
		}
		if (o == null || typeof this !== typeof o) {
			return false;
		}
		return this.qInternal.equals(o.qInternal);
	}
}