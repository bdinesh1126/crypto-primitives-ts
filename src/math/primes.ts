/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {checkArgument} from "../validation/preconditions";

export class Primes {

	private static MAX_INTEGER: number = 2147483647;

	/**
	 * Checks if the given number is a prime number. This is efficient for small primes only.
	 *
	 * @param num n, the number to be tested. Must be non-null and strictly positive. Restricted to the range of an int to guarantee that the
	 *               algorithm runs efficiently.
	 * @return true if n is prime, false otherwise.
	 * @throws NullPointerException     if n is null.
	 * @throws IllegalArgumentException if n is not strictly positive.
	 * @throws ArithmeticException      if n does not fit in an int (max is {@value Integer#MAX_VALUE}).
	 */
	static isSmallPrime(num: number): boolean {
		checkArgument(0 <= num, "The number to check should be positive");
		checkArgument(num <= this.MAX_INTEGER, `The number to check should not be bigger than ${this.MAX_INTEGER}`);
		const n = num;

		if (n === 1) {
			return false;
		} else if (n === 2 || n === 3) {
			return true;
		} else if (n % 2 == 0 || n % 3 == 0) {
			return false;
		} else {
			let i = 5;
			while (i <= Math.ceil(Math.sqrt(n))) {
				if (n % i == 0 || n % (i + 2) == 0) {
					return false;
				}
				i = i + 6;
			}
			return true;
		}
	}
}
