/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupElement} from "./group_element";
import {ImmutableBigInteger} from "../immutable_big_integer";

/**
 * Representation of a mathematical group.
 *
 * <p>MathematicalGroups are immutable.
 *
 * @param <G> self type.
 */
export interface MathematicalGroup<G extends MathematicalGroup<G>> {

	/**
	 * Checks whether a given value is a member of this <code>>MathematicalGroup</code>.
	 *
	 * @param value group element to check.
	 * @return true if the value is a member of the group and false otherwise.
	 */
	isGroupMember(value: ImmutableBigInteger): boolean;

	/**
	 * Returns the identity element of the group.
	 */
	identity: GroupElement<G>;

	/**
	 * Returns the q parameter, which is the order of the group.
	 */
	q: ImmutableBigInteger;

	/**
	 * Compares mathematical groups based on their order.
	 *
	 * @param other mathematical group
	 * @return true if both mathematical groups are of the same order, false otherwise.
	 */
	hasSameOrderAs(other: MathematicalGroup<G>): boolean;

	equals(group: G): boolean;
}