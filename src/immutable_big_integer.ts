/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Wraps a {@link BigInteger} to provide immutability.
 */
import {VerificatumBigInteger as BigInteger} from "./verificatum_big_integer";
import {verificatum} from 'vts';
import RandomSource = verificatum.base.RandomSource;
import {ImmutableUint8Array} from "./immutable_uint8Array";
import {cutToBitLength} from "./arrays";
import {byteArrayToInteger} from "./conversions";

export class ImmutableBigInteger {

    static ZERO: ImmutableBigInteger = new ImmutableBigInteger(BigInteger.ZERO);
    static ONE: ImmutableBigInteger = new ImmutableBigInteger(BigInteger.ONE);
    private readonly bigIntegerInternal: BigInteger;

    private constructor(value: BigInteger) {
        this.bigIntegerInternal = value;
    }

    // Public constructors

    /**
     * Copies an ImmutableBigInteger from the provided ImmutableBigInteger.
     *
     * @param value the ImmutableBigInteger from which to construct an ImmutableBigInteger.
     */
    static copy(value: ImmutableBigInteger): ImmutableBigInteger {
        return new this(value.bigIntegerInternal);
    }

    /**
     * Creates an ImmutableBigInteger from the provided number.
     *
     * @param value the number from which to construct an ImmutableBigInteger.
     */
    static fromNumber(value: number): ImmutableBigInteger {
        return new this(new BigInteger(value));
    }

    /**
     * Creates an ImmutableBigInteger from the provided string.
     *
     * @param value the string from which to construct an ImmutableBigInteger.
     * @param radix radix to be used in interpreting <code>value</code>
     */
    static fromString(value: string, radix: number = 10): ImmutableBigInteger {
        return new this(new BigInteger(value, radix));
    }

    /**
     * Constructs a randomly generated ImmutableBigInteger.
     *
     * @param numBits maximum bitLength of the new ImmutableBigInteger.
     * @param rng source of randomness to be used in computing the new ImmutableBigInteger
     */
    static random(numBits: number, rng: RandomSource): ImmutableBigInteger {
        const byteLength = Math.floor((numBits + 7) / 8);
        let r: ImmutableBigInteger;
        let rBytes: ImmutableUint8Array = cutToBitLength(ImmutableUint8Array.from(rng.getBytes(byteLength)), numBits);
        r = byteArrayToInteger(rBytes);
        return r;
    }

    // Methods delegation

    /**
     * Delegates the add function to the backing {@link BigInteger}.
     */
    public add(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.add(val.bigIntegerInternal));
    }

    /**
     * Delegates the bitLength function to the backing {@link BigInteger}.
     */
    public bitLength(): number {
        return this.bigIntegerInternal.bitLength();
    }

    /**
     * Delegates the compareTo function to the backing {@link BigInteger}.
     */
    public compareTo(val: ImmutableBigInteger): number {
        return this.bigIntegerInternal.compareTo(val.bigIntegerInternal);
    }

    /**
     * Delegates the equals function to the backing {@link BigInteger}.
     */
    public equals(val: ImmutableBigInteger): boolean {
        return this.bigIntegerInternal.equals(val.bigIntegerInternal);
    }

    /**
     * Delegates the gcd function to the backing {@link BigInteger}.
     */
    public gcd(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.gcd(val.bigIntegerInternal));
    }

    /**
     * Delegates the intValue function to the backing {@link BigInteger}.
     */
    public intValue(): number {
        return this.bigIntegerInternal.intValue();
    }

    /**
     * Computes the Jacobi symbol
     */
    public jacobi(other: ImmutableBigInteger): number {
        return this.bigIntegerInternal.jacobi(other.bigIntegerInternal);
    }

    /**
     * Delegates the mod function to the backing {@link BigInteger}.
     */
    public mod(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.mod(val.bigIntegerInternal));
    }

    /**
     * Delegates the modInverse function to the backing {@link BigInteger}.
     */
    public modInverse(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.modInverse(val.bigIntegerInternal));
    }

    /**
     * Delegates the modPow function to the backing {@link BigInteger}.
     */
    public modPow(exponent: ImmutableBigInteger, modulus: ImmutableBigInteger): ImmutableBigInteger {

        // Handle negative base and base >= modulus
        let b: ImmutableBigInteger;
        b = this.signum() < 0 || this.compareTo(modulus) >= 0 ? this.mod(modulus) : this;

        // Handle negative exponent
        let e: ImmutableBigInteger;
        if (exponent.signum() < 0) {
            e = exponent.negate();
            b = b.modInverse(modulus);
        } else {
            e = exponent;
        }

        return new ImmutableBigInteger(b.bigIntegerInternal.modPow(e.bigIntegerInternal, modulus.bigIntegerInternal));
    }

    /**
     * Delegates the modPowProd function to the backing {@link BigInteger}.
     */
    public static modPowProd(bases: ImmutableBigInteger[], exponents: ImmutableBigInteger[], modulus: ImmutableBigInteger): ImmutableBigInteger {
        const basesConverted = bases.map(b => b.toBigInteger());
        const exponentsConverted = exponents.map(e => e.toBigInteger());
        const modulusConverted = modulus.toBigInteger();

        return new ImmutableBigInteger(BigInteger.modPowProd(basesConverted, exponentsConverted, modulusConverted));
    }

    /**
     * Delegates the multiply function to the backing {@link BigInteger}.
     */
    public multiply(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.multiply(val.bigIntegerInternal));
    }

    /**
     * Delegates the divide function to the backing {@link BigInteger}.
     */
    public divide(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.divide(val.bigIntegerInternal));
    }

    /**
     * Delegates the negate function to the backing {@link BigInteger}.
     */
    public negate(): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.negate());
    }

    /**
     * Delegates the signum function to the backing {@link BigInteger}.
     */
    public signum(): number {
        return this.bigIntegerInternal.signum();
    }

    /**
     * Delegates the subtract function to the backing {@link BigInteger}.
     */
    public subtract(val: ImmutableBigInteger): ImmutableBigInteger {
        return new ImmutableBigInteger(this.bigIntegerInternal.subtract(val.bigIntegerInternal));
    }

    /**
     * Delegates the testBit function to the backing {@link BigInteger}.
     */
    public testBit(n: number): boolean {
        return this.bigIntegerInternal.testBit(n);
    }

    /**
     * Delegates the toByteArray function to the backing {@link BigInteger}.
     */
    public toByteArray(): number[] {
        return [...this.bigIntegerInternal.toByteArray()];
    }

    /**
     * Delegates the toString function to the backing {@link BigInteger}.
     * @param r the radix for the string representation. If radix is omitted or defined outside the range from 2 to 36 inclusive, it will default to 10.
     */
    public toString(r?: number): string {
        let radix: number = r;
        if (r == null || r < 2 || r > 36) {
            radix = 10;
        }
        return this.bigIntegerInternal.toString(radix);
    }

    /**
     * Returns a {@link BigInteger} from the backing {@link BigInteger}.
     */
    public toBigInteger(): BigInteger {
        return new BigInteger(this.bigIntegerInternal.toString(16));
    }

}