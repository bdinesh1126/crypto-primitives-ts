/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "./validation/preconditions";

/**
 * Wraps a Uint8Array to provide immutability.
 */
export class ImmutableUint8Array {

	private readonly arrayInternal: Uint8Array;

	private constructor(array: Uint8Array) {
		this.arrayInternal = array;
	}

	/**
	 * Creates an ImmutableUint8Array from the provided ArrayLike.
	 *
	 * @param arrayLike the array from which to construct an immutable array.
	 * @throws NullPointerError if arrayLike is null.
	 * @throws IllegalArgumentError if arrayLike contains nulls.
	 */
	public static from(arrayLike: ArrayLike<number>): ImmutableUint8Array {
		checkNotNull(arrayLike);
		checkArgument(Object.values(arrayLike).every(e => e != null), "The array must not contain nulls");

		return new ImmutableUint8Array(Uint8Array.from(arrayLike));
	}

	get length() {
		return this.arrayInternal.length;
	}


	/**
	 * Returns the element at index.
	 *
	 * @param index the index at which to retrieve an element of the array.
	 */
	public get(index: number) {
		return this.arrayInternal[index];
	}

	/**
	 * Use with caution, using this by-passes the immutability of this class.
	 */
	public value() {
		return Uint8Array.from(this.arrayInternal);
	}

	/**
	 * Converts this to a plain Array.
	 * Use with caution, using this by-passes the immutability of this class.
	 */
	public toArray(): Array<number> {
		return Array.from(this.arrayInternal);
	}

}