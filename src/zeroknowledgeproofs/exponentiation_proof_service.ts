/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ZqElement} from "../math/zq_element";
import {GqGroup} from "../math/gq_group";
import {GqElement} from "../math/gq_element";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {GroupVector} from "../group_vector";
import {ExponentiationProof} from "./exponentiation_proof";
import {ImmutableArray} from "../immutable_array";
import {ZqGroup} from "../math/zq_group";
import {ImmutableBigInteger} from "../immutable_big_integer";
import {RandomService} from "../math/random_service";
import {HashService} from "../hashing/hash_service";
import {Hashable} from "../hashing/hashable";
import {byteArrayToInteger} from "../conversions";
import {BYTE_SIZE} from "../constants";

export class ExponentiationProofService {

	private static readonly EXPONENTIATION_PROOF = "ExponentiationProof";

	private randomServiceInternal: RandomService;
	private hashServiceInternal: HashService;

	public constructor(randomService: RandomService, hashService: HashService) {
		this.randomServiceInternal = checkNotNull(randomService);
		this.hashServiceInternal = checkNotNull(hashService);
	}

	/**
	 * Computes an image of a phi-function for exponentiation given a preimage and bases g<sub>0</sub>, ..., g<sub>n-1</sub>.
	 *
	 * @param preimage x ∈ Z<sub>q</sub>. Not null.
	 * @param bases    (g<sub>0</sub>, ..., g<sub>n-1</sub>) ∈ G<sub>q</sub><sup>n</sup>. Not null and not empty.
	 * @return an image (y<sub>0</sub>, ..., y<sub>n-1</sub>) ∈ G<sub>q</sub><sup>n</sup>
	 * @throws NullPointerError    if any of the parameters are null
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                      <li>the bases are empty</li>
	 *                                      <li>the preimage does not have the same group order as the bases</li>
	 *                                  </ul>
	 */
	static computePhiExponentiation(preimage: ZqElement, bases: GroupVector<GqElement, GqGroup>): GroupVector<GqElement, GqGroup> {
		checkNotNull(preimage);
		checkNotNull(bases);

		checkArgument(bases.size !== 0, "The vector of bases must contain at least 1 element.");
		checkArgument(preimage.group.hasSameOrderAs(bases.group), "The preimage and the bases must have the same group order.");

		const x: ZqElement = preimage;
		const g: GroupVector<GqElement, GqGroup> = bases;

		return GroupVector.from(g.elements.map(_ => _.exponentiate(x)));
	}

	/**
	 * Generates a proof of validity for the provided exponentiations.
	 *
	 * @param bases                <b>g</b> ∈ G<sub>q</sub><sup>n</sup>. Not null and not empty.
	 * @param exponent             x ∈ Z<sub>q</sub>, a secret exponent. Not null.
	 * @param exponentiations      <b>y</b> ∈ G<sub>q</sub><sup>n</sup>. Not null and not empty.
	 * @param auxiliaryInformation i<sub>aux</sub>, auxiliary information to be used for the hash. Must be non null and not contain nulls. Can be
	 *                             empty.
	 * @return an exponentiation proof
	 * @throws NullPointerError     if any of the parameters are null.
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                  	 <li>the bases and the exponentiations do not have the same group</li>
	 *                                  	 <li>the bases and the exponentiations do not have the same size</li>
	 *                                  	 <li>the exponent does not have the same group order as the exponentiations</li>
	 *                                  </ul>
	 */
	genExponentiationProof(bases: GroupVector<GqElement, GqGroup>, exponent: ZqElement, exponentiations: GroupVector<GqElement, GqGroup>,
						   auxiliaryInformation: string[]): ExponentiationProof {

		checkNotNull(bases);
		checkNotNull(exponent);
		checkNotNull(exponentiations);
		checkNotNull(auxiliaryInformation);
		checkArgument(auxiliaryInformation.every(auxInfo => auxInfo != null), "The auxiliary information must not contain null objects.");

		const i_aux = ImmutableArray.from(auxiliaryInformation);
		const g: GroupVector<GqElement, GqGroup> = bases;
		const x: ZqElement = exponent;
		const y: GroupVector<GqElement, GqGroup> = exponentiations;
		const zqGroup: ZqGroup = x.group;

		// Cross-dimension checks
		checkArgument(g.size !== 0, "The bases must contain at least 1 element.");
		checkArgument(g.size === y.size, "Bases and exponentiations must have the same size.");

		// Cross-group checks
		checkArgument(g.group.equals(y.group), "Bases and exponentiations must have the same group.");
		checkArgument(x.group.hasSameOrderAs(y.group),
			"The exponent and the exponentiations must have the same group order.");

		// By construction, we assume that the precondition y_i = g_i^x holds and, thus, we avoid to explicitly check it for performance reasons.
		// The proof would not verify otherwise.

		// Context
		const p: ImmutableBigInteger = g.group.p;
		const q: ImmutableBigInteger = g.group.q;
		checkArgument(this.hashServiceInternal.getHashLength() * BYTE_SIZE < q.bitLength(),
			"The hash service's bit length must be smaller than the bit length of q.");

		// Operations.
		const bValue: ImmutableBigInteger = this.randomServiceInternal.genRandomInteger(q);
		const b = ZqElement.create(bValue, zqGroup);
		const c: GroupVector<GqElement, GqGroup> = ExponentiationProofService.computePhiExponentiation(b, g);
		const f: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(p, q, g.toHashableForm());

		let h_aux: ImmutableArray<Hashable>;
		if (i_aux.length != 0) {
			h_aux = ImmutableArray.of<Hashable>(ExponentiationProofService.EXPONENTIATION_PROOF, i_aux);
		} else {
			h_aux = ImmutableArray.of<Hashable>(ExponentiationProofService.EXPONENTIATION_PROOF);
		}

		const eValue = byteArrayToInteger(this.hashServiceInternal.recursiveHash(f, y.toHashableForm(), c.toHashableForm(), h_aux));
		const e = ZqElement.create(eValue, zqGroup);
		const z = b.add(e.multiply(x));

		return new ExponentiationProof(e, z);
	}

	/**
	 * Verifies the validity of a given {@link ExponentiationProof}.
	 *
	 * @param bases                g, the bases that were used to generate the proof. Must be non null.
	 * @param exponentiations      y, the exponentiations the were used to generate the proof. Must be non null.
	 * @param proof                (e, z), the proof to be verified
	 * @param auxiliaryInformation i<sub>aux</sub>, auxiliary information that was used during proof generation. Must be non null and not contain
	 *                             nulls.
	 * @return {@code true} if the exponentiation proof is valid, {@code false} otherwise.
	 * @throws NullPointerException     if any of the bases, exponentiations, or proof is null
	 * @throws IllegalArgumentError if
	 *                                  <ul>
	 *                                      <li>the auxiliary information contains null elements</li>
	 *                                      <li>the bases are empty</li>
	 *                                      <li>the exponentations do not have the same size as the bases</li>
	 *                                      <li>the bases and the exponentiations do not have the same group</li>
	 *                                      <li>the exponentiation proof does not have the same group order as the bases and the exponentiations</li>
	 *                                      <li>the group order q's bit length is smaller than the hash service's hash length</li>
	 *                                  </ul>
	 */
	verifyExponentiation(bases: GroupVector<GqElement, GqGroup>, exponentiations: GroupVector<GqElement, GqGroup>,
						 proof: ExponentiationProof, auxiliaryInformation: string[]): boolean {
		checkNotNull(bases);
		checkNotNull(exponentiations);
		checkNotNull(proof);
		checkNotNull(auxiliaryInformation);

		checkArgument(auxiliaryInformation.every(aux_info => aux_info != null), "The auxiliary information must not contain null elements.");

		const i_aux: ImmutableArray<string> = ImmutableArray.from(auxiliaryInformation);
		const g: GroupVector<GqElement, GqGroup> = bases;
		const y: GroupVector<GqElement, GqGroup> = exponentiations;
		const e: ZqElement = proof.e;
		const z: ZqElement = proof.z;
		const n: number = g.size;

		// Cross-dimension checking.
		checkArgument(g.elementSize > 0, "The bases must contain at least 1 element.");
		checkArgument(g.size === y.size, "Bases and exponentiations must have the same size.");

		// Cross-group checking.
		checkArgument(g.group.equals(y.group), "Bases and exponentiations must belong to the same group.");
		checkArgument(proof.group.hasSameOrderAs(bases.group), "The proof must have the same group order as the bases.");

		// Context.
		const zqGroup: ZqGroup = e.group;
		const p: ImmutableBigInteger = g.group.p;
		const q: ImmutableBigInteger = g.group.q;
		checkArgument(this.hashServiceInternal.getHashLength() * BYTE_SIZE < q.bitLength(),
			"The hash service's bit length must be smaller than the bit length of q.");

		// Operations.
		const x: GroupVector<GqElement, GqGroup> = ExponentiationProofService.computePhiExponentiation(z, g);
		const f: ImmutableArray<Hashable> = ImmutableArray.of<Hashable>(p, q, g.toHashableForm());

		const result: GqElement[] = [];
		for (let i = 0; i < n; i++) {
			result[i] = x.get(i).multiply(y.get(i).invert().exponentiate(e));
		}

		const c_prime: GroupVector<GqElement, GqGroup> = GroupVector.from(result);

		let h_aux: ImmutableArray<Hashable>;
		if (i_aux.length !== 0) {
			h_aux = ImmutableArray.of<Hashable>(ExponentiationProofService.EXPONENTIATION_PROOF, i_aux);
		} else {
			h_aux = ImmutableArray.of<Hashable>(ExponentiationProofService.EXPONENTIATION_PROOF);
		}

		const e_prime_value: ImmutableBigInteger = byteArrayToInteger(this.hashServiceInternal.recursiveHash(f, y.toHashableForm(), c_prime.toHashableForm(), h_aux));
		const e_prime: ZqElement = ZqElement.create(e_prime_value, zqGroup);

		return e.equals(e_prime);
	}

}
