/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "./validation/preconditions";
import {ImmutableUint8Array} from "./immutable_uint8Array";
import {ImmutableBigInteger} from "./immutable_big_integer";
import * as UTF8 from "utf8";
import {IllegalArgumentError} from "./error/illegal_argument_error";

/**
 * Converts a byte array to its {@link ImmutableBigInteger} equivalent.
 *
 * @param bytes B, the byte array to convert. Must be non-null and non-empty.
 * @return an ImmutableBigInteger corresponding to the provided byte array representation.
 */
export function byteArrayToInteger(bytes: ImmutableUint8Array): ImmutableBigInteger {
	checkNotNull(bytes);
	checkArgument(bytes.length > 0, "The byte array to convert must be non-empty.");

	let x: ImmutableBigInteger = ImmutableBigInteger.ZERO;
	const n = bytes.length;
	const twoHundredFiftySix = ImmutableBigInteger.fromString("256");
	for (let i = 0; i < n; i++) {
		x = twoHundredFiftySix.multiply(x).add(ImmutableBigInteger.fromString(bytes.get(i).toString()));
	}

	return x;
}

/**
 * Converts an {@link ImmutableBigInteger} to a byte array representation.
 * <p>
 * NOTE: our implementation slightly deviates from the specifications for performance reasons. Benchmarks show that our implementation is orders
 * of magnitude faster than the pseudo-code implementation integerToByteArraySpec. Both implementations are equivalent, and we have a unit test
 * ensuring it.
 * </p>
 *
 * @param x the positive BigInteger to convert.
 * @return the byte array representation of this BigInteger as {@link Uint8Array}.
 */
export function integerToByteArray(x: ImmutableBigInteger): ImmutableUint8Array {
	checkNotNull(x);
	checkArgument(x.compareTo(ImmutableBigInteger.ZERO) >= 0);

	const twosComplement = x.toByteArray();
	let result: ImmutableUint8Array;

	if (twosComplement[0] === 0 && twosComplement.length > 1) {
		result = ImmutableUint8Array.from(twosComplement.slice(1));
	} else {
		result = ImmutableUint8Array.from(twosComplement);
	}

	return result;
}

/**
 * Converts a string to a byte array representation as {@link ImmutableUint8Array}.
 *
 * @param s S, the string to convert.
 * @return the byte array representation of the string.
 */
export function stringToByteArray(s: string): ImmutableUint8Array {
	checkNotNull(s);

	try {
		UTF8.encode(s);
	} catch (e) {
		throw new IllegalArgumentError("The string does not correspond to a valid sequence of UTF-8 encoding.")
	}

	// Corresponds to UTF-8(S)
	return ImmutableUint8Array.from(new TextEncoder().encode(s));
}

/**
 * Converts a decimal string representation to an {@link ImmutableBigInteger} representation.
 *
 * @param s S, the decimal string representation to convert. Not Null, not empty and all characters must be decimal characters.
 * @return x, the {@link ImmutableBigInteger} representation of the string.
 * @throws NullPointerError if the string is null.
 * @throws IllegalArgumentError if the string s is empty or not a valid decimal representation of an ImmutableBigInteger.
 */
export function stringToInteger(s: string): ImmutableBigInteger {
	checkNotNull(s);
	checkArgument(s.length > 0, "The string to convert cannot be empty.");
	checkArgument(s.match("^\\d+$") != null, "The string to convert is not a valid decimal representation of an ImmutableBigInteger.");

	// Corresponds to UTF-8^-1(B)
	return ImmutableBigInteger.fromString(s, 10);
}

/**
 * Converts a {@link ImmutableBigInteger} representation to a string representation.
 *
 * @param x, the {@link ImmutableBigInteger} representation to convert. Not Null, positive (including 0).
 * @return S, the string representation of the {@link ImmutableBigInteger}.
 * @throws NullPointerError if the {@Link ImmutableBigInteger} is null.
 * @throws IllegalArgumentError if the {@Link ImmutableBigInteger} x is not positive.
 */
export function integerToString(x: ImmutableBigInteger): string {
    checkNotNull(x);
    checkArgument(x.compareTo(ImmutableBigInteger.ZERO) >= 0);

	// Corresponds to Decimal^-1(x)
    return x.toString(10);
}
