/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Defines the context parameters for the Argon2id algorithm.
 */
export interface Argon2Context {
	/**
	 * The <b>memory usage parameter</b>, defines the memory usage for argon2id. Usage is 2^m KiB.
	 */
	m: number
	/**
	 * The <b>parallelism parameter</b>, defines the number of lanes used by Argon2.
	 */
	p: number
	/**
	 * The <b>iteration count</b> parameter, influences the running time of Argon2
	 */
	i: number
}