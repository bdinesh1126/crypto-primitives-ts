/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ImmutableArray} from "../immutable_array";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {ImmutableBigInteger} from "../immutable_big_integer";

/**
 * Recursive union type representing the type of input that can be passed to the {@link HashService#recursiveHash} algorithm.
 */
export type Hashable = ImmutableUint8Array | string | ImmutableBigInteger | ImmutableArray<Hashable>;