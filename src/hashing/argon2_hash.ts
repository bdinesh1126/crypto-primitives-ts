/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ImmutableUint8Array} from "../immutable_uint8Array";
import {checkNotNull} from "../validation/preconditions";

/**
 * An argon2 hash composed of a tag and a salt.
 *
 * <p>Instances of this class are immutable.</p>
 */
export class Argon2Hash {
	private readonly tagInternal: ImmutableUint8Array;
	private readonly saltInternal: ImmutableUint8Array;

	constructor(tag: ImmutableUint8Array, salt: ImmutableUint8Array) {
		this.tagInternal = checkNotNull(tag);
		this.saltInternal = checkNotNull(salt);
	}

	get tag(): ImmutableUint8Array {
		return this.tagInternal;
	}

	get salt(): ImmutableUint8Array {
		return this.saltInternal;
	}
}