/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {argon2id} from "hash-wasm"
import {SecureRandomGenerator} from "../generator/secure_random_generator";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {Argon2Hash} from "./argon2_hash";
import {Argon2Context} from "./argon2_context";

export class Argon2Service {
	public static readonly SALT_LENGTH_BYTES = 16;
	public static readonly TAG_LENGTH_BYTES = 32;

	/**
	 * Creates an instance of Argon2Service with the required configuration values in the context
	 * @param context the configuration to use
	 * @throws {@link NullPointerError} if any of the arguments is null.
	 * @throws {@link IllegalArgumentError} if the
	 * 	<ul>
	 * 		<li>memory is not in the expected range of [14, 24]</li>
	 * 		<li>parallelism is not in the expected range of [1, 16]</li>
	 * 		<li>iteration count is not in the expected range of [1, 256]</li>
	 * 	</ul>
	 */
	public constructor(private context: Argon2Context) {
		checkNotNull(context.m);
		checkNotNull(context.p);
		checkNotNull(context.i);
		checkArgument(14 <= context.m && context.m <= 24, "memory outside of expected range");
		checkArgument(1 <= context.p && context.p <= 16, "parallelism outside of expected range");
		checkArgument(1 <= context.i && context.i <= 256, "iteration count outside of expected range");
	}

	private static async argon2id(c: Argon2Config, k: ImmutableUint8Array): Promise<ImmutableUint8Array> {
		const options = {
			hashLength: c.tagLength,
			salt: c.salt,
			memorySize: c.memory,
			parallelism: c.parallelism,
			iterations: c.iterations,
			password: k.value(),
			outputType: "binary"
		};

		// @ts-ignore
		const t = await argon2id(options);
		// @ts-ignore
		return ImmutableUint8Array.from(t);
	}

	/**
	 * Computes the Argon2id tag of the input keying material.
	 *
	 * @param inputKeyingMaterial k ∈ B<sup>*</sup>.
	 * @return The tag and the salt represented as a {@link Argon2Hash} (t,s) ∈ B<sup>32</sup> × B<sup>16</sup>.
	 * @throws {@link NullPointerError} if the input keying material is null.
	 */
	public async genArgon2id(inputKeyingMaterial: ImmutableUint8Array): Promise<Argon2Hash> {
		const k = checkNotNull(inputKeyingMaterial);

		const s = SecureRandomGenerator.genRandomBytes(Argon2Service.SALT_LENGTH_BYTES);
		const t = await this.getArgon2id(k, s);

		return new Argon2Hash(t, s);
	}

	/**
	 * Computes the Argon2id tag of the input keying material and the given salt.
	 *
	 * @param inputKeyingMaterial k ∈ B<sup>*</sup>.
	 * @param salt                s k ∈ B<sup>16</sup>.
	 * @return The tag t ∈ B<sup>32</sup>.
	 * @throws {@link NullPointerError} if any input is null.
	 */
	public getArgon2id(inputKeyingMaterial: ImmutableUint8Array, salt: ImmutableUint8Array): Promise<ImmutableUint8Array> {
		const k = checkNotNull(inputKeyingMaterial);
		const s = checkNotNull(salt);
		const m: number = this.context.m;
		const p: number = this.context.p;
		const i: number = this.context.i;

		const c: Argon2Config = {
			tagLength: Argon2Service.TAG_LENGTH_BYTES,
			salt: s.value(),
			memory: 2 ** m,
			parallelism: p,
			iterations: i
		}
		return Argon2Service.argon2id(c, k);
	}

}
