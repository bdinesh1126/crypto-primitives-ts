/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Wraps a simple array to provide immutability.
 */
export class ImmutableArray<T> {

	private readonly elementsInternal: T[]

	private constructor(...elements: T[]) {
		this.elementsInternal = [...elements];
	}

	/**
	 * Constructs an immutable array with the provided elements.
	 *
	 * @param elements the elements backing the immutable array.
	 */
	static of<T>(...elements: T[]) {
		return new ImmutableArray<T>(...elements);
	}

	/**
	 * Constructs an immutable array with the provided array.
	 *
	 * @param elements the array backing the immutable array.
	 */
	static from<T>(elements: T[]): ImmutableArray<T> {
		return new ImmutableArray<T>(...elements);
	}

	get length() {
		return this.elementsInternal.length;
	}

	/**
	 * Returns the element at index.
	 *
	 * @param index the index at which to retrieve an element of the array.
	 */
	public get(index: number): T {
		return this.elementsInternal[index];
	}

	/**
	 * Returns a shallow copy of the backing array.
	 */
	public elements(): T[] {
		return Array.from(this.elementsInternal);
	}

	/**
	 * Delegates the map function to the backing array. This does not modify the array.
	 */
	public map<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): ImmutableArray<U> {
		return ImmutableArray.from(this.elementsInternal.map(callbackfn, thisArg));
	}

	/**
	 * Delegates the filter function to the backing array. This does not modify the array.
	 */
	public filter(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): ImmutableArray<T> {
		return ImmutableArray.from(this.elementsInternal.filter(predicate, thisArg));
	}

}