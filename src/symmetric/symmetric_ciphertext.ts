/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ImmutableUint8Array} from "../immutable_uint8Array";
import {checkNotNull} from "../validation/preconditions";

/**
 * A symmetric ciphertext composed of a ciphertext and nonce.
 *
 * <p>Instances of this class are immutable.</p>
 */
export class SymmetricCiphertext {
	private readonly ciphertextInternal: ImmutableUint8Array;
	private readonly nonceInternal: ImmutableUint8Array;

	constructor(ciphertext: ImmutableUint8Array, nonce: ImmutableUint8Array) {
		this.ciphertextInternal = checkNotNull(ciphertext);
		this.nonceInternal = checkNotNull(nonce);
	}

	get ciphertext(): ImmutableUint8Array {
		return this.ciphertextInternal;
	}

	get nonce(): ImmutableUint8Array {
		return this.nonceInternal;
	}
}