/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as crypto from "crypto";
import {checkArgument, checkNotNull} from "../validation/preconditions";
import {SecureRandomGenerator} from "../generator/secure_random_generator";
import {SymmetricEncryptionAlgorithm} from "./symmetric_encryption_algorithm";
import {ImmutableUint8Array} from "../immutable_uint8Array";
import {concat} from "../arrays";
import {stringToByteArray} from "../conversions";
import {SymmetricCiphertext} from "./symmetric_ciphertext";

export class SymmetricAuthenticatedEncryptionService {
	private readonly algorithmInternal: SymmetricEncryptionAlgorithm;

	constructor(algorithm: SymmetricEncryptionAlgorithm) {
		this.algorithmInternal = algorithm
	}

	async genCiphertextSymmetric(encryptionKey: ImmutableUint8Array, plaintext: ImmutableUint8Array, associatedData: string[]): Promise<SymmetricCiphertext> {
		checkNotNull(encryptionKey);
		checkNotNull(plaintext);
		checkNotNull(associatedData);
		checkArgument(associatedData.every(data => data !== null), "The associated data must not contain null objects.");

		checkArgument(associatedData.every(data => stringToByteArray(data).length <= 255), "The length of each associated data must be smaller or equal to 255.");

		const K: ImmutableUint8Array = encryptionKey;
		const P: ImmutableUint8Array = plaintext;

		const nonce: ImmutableUint8Array = SecureRandomGenerator.genRandomBytes(this.algorithmInternal.nonceLength);
		const associated: ImmutableUint8Array = concat(...associatedData
			.map(associated_i => stringToByteArray(associated_i))
			.map(associated_i_bytes => concat(ImmutableUint8Array.from([associated_i_bytes.length]), associated_i_bytes)));
		const C: ImmutableUint8Array = await this.authenticatedEncryption(K, nonce, P, associated);

		return new SymmetricCiphertext(C, nonce);
	}

	async getPlaintextSymmetric(encryptionKey: ImmutableUint8Array, ciphertext: ImmutableUint8Array, nonce: ImmutableUint8Array,
								associatedData: string[]): Promise<ImmutableUint8Array> {
		checkNotNull(encryptionKey);
		checkNotNull(ciphertext);
		checkNotNull(nonce);
		checkNotNull(associatedData);
		checkArgument(associatedData.every(data => data !== null), "The associated data must not contain null objects.");

		checkArgument(associatedData.every(data => stringToByteArray(data).length <= 255), "The length of each associated data must be smaller or equal to 255.");

		const K: ImmutableUint8Array = encryptionKey;
		const C: ImmutableUint8Array = ciphertext;

		const associated: ImmutableUint8Array = concat(...associatedData
			.map(associated_i => stringToByteArray(associated_i))
			.map(associated_i_bytes => concat(ImmutableUint8Array.from([associated_i_bytes.length]), associated_i_bytes)));

		return this.authenticatedDecryption(K, nonce, C, associated);
	}


	async authenticatedEncryption(encryptionKey: ImmutableUint8Array, nonce: ImmutableUint8Array, plaintext: ImmutableUint8Array,
								  associatedData: ImmutableUint8Array): Promise<ImmutableUint8Array> {
		if (typeof window !== "undefined" && window.crypto && window.crypto.subtle && window.crypto.subtle.encrypt) {
			const key = await window.crypto.subtle.importKey("raw", encryptionKey.value(), {name: "AES-GCM", length: 256}, false, ["encrypt"]);
			const ciphertext = await window.crypto.subtle.encrypt({
					name: "AES-GCM",
					additionalData: associatedData.value(),
					iv: nonce.value(),
					tagLength: 128
				},
				key,
				plaintext.value());

			return ImmutableUint8Array.from(new Uint8Array(ciphertext));
		}

		if (crypto && crypto.createCipheriv) {
			const cipher = crypto.createCipheriv("aes-256-gcm", encryptionKey.value(), nonce.value(), {
				authTagLength: 16
			});
			cipher.setAAD(associatedData.value());
			const ciphertext = Buffer.concat([cipher.update(plaintext.value()), cipher.final(), cipher.getAuthTag()]);

			return ImmutableUint8Array.from(ciphertext);
		}

		throw new Error(`"Crypto" is not available`);
	}

	async authenticatedDecryption(encryptionKey: ImmutableUint8Array, nonce: ImmutableUint8Array, ciphertextWithAuthTag: ImmutableUint8Array,
								  associatedData: ImmutableUint8Array): Promise<ImmutableUint8Array> {
		if (typeof self !== "undefined" && self.crypto && self.crypto.subtle && self.crypto.subtle.decrypt) {
			const key = await self.crypto.subtle.importKey("raw", encryptionKey.value(), {name: "AES-GCM", length: 256}, false, ["decrypt"]);
			const plaintext = await self.crypto.subtle.decrypt({
					name: "AES-GCM",
					additionalData: associatedData.value().buffer,
					iv: nonce.value().buffer,
					tagLength: 128
				},
				key,
				ciphertextWithAuthTag.value().buffer);

			return ImmutableUint8Array.from(new Uint8Array(plaintext));
		}

		if (crypto && crypto.createDecipheriv) {
			let decipher = crypto.createDecipheriv("aes-256-gcm", encryptionKey.value(), nonce.value(), {
				authTagLength: 16
			});
			const rawCiphertext = Buffer.from(ciphertextWithAuthTag.value());
			const authTag: Uint8Array = rawCiphertext.subarray(rawCiphertext.length - 16);
			const ciphertext: Uint8Array = rawCiphertext.subarray(0, rawCiphertext.length - 16);

			decipher.setAAD(Buffer.from(associatedData.value()));
			decipher.setAuthTag(authTag);

			let plaintext = decipher.update(ciphertext);
			return ImmutableUint8Array.from(Buffer.concat([plaintext, decipher.final()]));
		}

		throw new Error(`"Crypto" is not available`);
	}


}