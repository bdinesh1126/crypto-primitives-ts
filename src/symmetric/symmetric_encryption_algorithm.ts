/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {CipherGCMTypes} from "crypto";

export class SymmetricEncryptionAlgorithm {
	private readonly algorithmNameInternal: CipherGCMTypes;
	private readonly nonceLengthInternal: number;

	private constructor(algorithmName: CipherGCMTypes, nonceLength: number) {
		this.algorithmNameInternal = algorithmName;
		this.nonceLengthInternal = nonceLength;
	}

	get algorithmName(): CipherGCMTypes {
		return this.algorithmNameInternal;
	}

	get nonceLength(): number {
		return this.nonceLengthInternal;
	}

	public static AES256_GCM_NOPADDING(): SymmetricEncryptionAlgorithm {
		return new SymmetricEncryptionAlgorithm('aes-256-gcm', 12);
	}
}