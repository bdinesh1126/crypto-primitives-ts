# Crypto-Primitives TypeScript

## What is the content of this repository?

The [Crypto-Primitives library](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives) highlights the importance of a robust and
misuse-resistant library for cryptographic algorithms. However, since the Crypto-Primitives implements the algorithms in Java, we require a TypeScript
implementation suitable for Frontend applications, notably in
the [voting-client-js](https://gitlab.com/swisspost-evoting/e-voting/e-voting/-/tree/master/voting-client-js).

The Crypto-Primitives repository contains
the [pseudo-code specification](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives/-/blob/master/Crypto-Primitives-Specification.pdf)
of the algorithms.
We do not require all algorithms in TypeScript.
Currently, the TypeScript library contains the following algorithms.

* ElGamal Encryption Scheme
* Plaintext Equality and Exponentation Zero-Knowledge Proofs
* Fixed-length and variable-length hash functions
* Probabilistic primality tests
* Authenticated symmetric encryption methods

## Under which license is this code available?

The Crypto-primitives TypeScript library is released under Apache 2.0.

## Code Quality

We strive for excellent code quality to minimize the risk of bugs and vulnerabilities. We rely on the following tools for code analysis.

| Tool                                                                                    | Focus                                                                                              |
|-----------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| [SonarQube](https://www.sonarqube.org/)                                                 | Code quality and code security                                                                     |
| [Fortify](https://www.microfocus.com/de-de/products/static-code-analysis-sast/overview) | Static Application Security Testing                                                                |
| [JFrog X-Ray](https://jfrog.com/xray/)                                                  | Common vulnerabilities and exposures (CVE) analysis, Open-source software (OSS) license compliance | |

### SonarQube Analysis

We parametrize SonarQube with the built-in Sonar way quality profile. The SonarQube analysis of the crypto-primitives-ts code reveals 0 bugs, 0
vulnerabilities, 0 security hotspots, and 0 code smells.

![SonarQube](SonarQube.jpg)

The code smell concern methods for probabilistic primality testing with too high complexity. We prefer to keep the methods aligned to the relevant
standards at the cost of increased cognitive complexity.

Moreover, a high test coverage illustrates that we extensively test the crypto-primitives library.

### Fortify Analysis

The Fortify analysis showed 0 critical, 0 high, 0 medium, and 4 low criticality issues. We manually reviewed all 4 low-criticality issues and assessed
them as false positives.

### JFrog X-Ray Analysis

The X-Ray analysis indicates that none of the crypto-primitives TypeScript' 3rd party dependencies contains known vulnerabilities or non-compliant
open source software licenses. As a general principle, we try to minimize external dependencies in cryptographic libraries and only rely on
well-tested and widely used 3rd party components.

## Mathematical Variables Naming Convention

See the crypto-primitives readme for a thorough explanation of our [naming convention](https://gitlab.com/swisspost-evoting/crypto-primitives/crypto-primitives#mathematical-variables-naming-convention).

## Changelog

An overview of all major changes within the published releases is available [here.](CHANGELOG.md)

## Additional documentation

You can find additional documents related to the crypto-primitives in the following locations:

| Repositories                                                                                                                                                                | Content                                                                                          |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------|
| [`System specification`](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/System/System_Specification.pdf)                                | System Specification of the e-voting system.                                                     |
| [`Voting protocol`](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/Protocol/Swiss_Post_Voting_Protocol_Computational_proof.pdf)         | The cryptographic protocol that describes the Swiss Post e-voting system in a mathematical form. |
| [`Voting System architecture`](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation/-/blob/master/System/SwissPost_Voting_System_architecture_document.pdf) | Architecture documentation of the e-voting system.                                               |
| [`Documentation overview`](https://gitlab.com/swisspost-evoting/e-voting/e-voting-documentation)                                                                            | Overview of all documentations.                                                                  |

## Acknowledgements

This software uses the Verificatum TypeScript Big Integer library, https://github.com/verificatum/verificatum-vts-ba, implemented by Douglas Wikström
and made publicly available by Verificatum AB with the financial support of Swiss Post under MIT License. We want to thank Douglas Wikström for his
kind support.
