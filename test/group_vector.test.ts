/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {TestGroup} from "./tools/math/test_group";
import {TestGroupElement} from "./tools/test_group_element";
import {GroupVector} from "../src/group_vector";
import {IllegalArgumentError} from "../src/error/illegal_argument_error";
import {IllegalStateError} from "../src/error/illegal_state_error";
import {RandomService} from "../src/math/random_service";
import {TestValuedElement} from "./tools/math/test_valued_element";
import {ImmutableBigInteger} from "../src/immutable_big_integer";
import {ZqGroup} from "../src/math/zq_group";
import {ZqElement} from "../src/math/zq_element";
import {GqGroup} from "../src/math/gq_group";

describe("GroupVector method validations", () => {
  test("of with valid parameters", () => {
    const group: TestGroup = new TestGroup();
    const e1: TestGroupElement = new TestGroupElement(group);
    const e2: TestGroupElement = new TestGroupElement(group);

    const groupVector: GroupVector<TestGroupElement, TestGroup> = GroupVector.of(e1, e2);
    expect(2).toEqual(groupVector.size);

    const emptyGroupVector: GroupVector<TestGroupElement, TestGroup> = GroupVector.of();
    expect(0).toEqual(emptyGroupVector.size);
  });

  test("of with invalid parameters throws", () => {
    const error = new IllegalArgumentError("Elements must not contain nulls");

    expect(() => GroupVector.of(null)).toThrow(error);
    const e1: TestGroupElement = new TestGroupElement(new TestGroup());
    expect(() => GroupVector.of(e1, null)).toThrow(error);
  });

  test("of with empty element", () => {
    expect(() => GroupVector.of()).not.toThrow();

    const vector: GroupVector<TestGroupElement, TestGroup> = GroupVector.of();
    expect(0).toEqual(vector.size);

    expect(() => vector.group).toThrow(IllegalStateError);
  });

  test("from with null throws", () => {
    expect(() => GroupVector.from([null])).toThrow(IllegalArgumentError);
  });

  test("from with valid and null values throws", () => {
    const validElement: TestGroupElement = new TestGroupElement(new TestGroup());
    expect(() => GroupVector.from([validElement, null])).toThrow(IllegalArgumentError);
  });

  test("from with elements of different groups throws", () => {
    const group1: TestGroup = new TestGroup();
    const first: TestGroupElement = new TestGroupElement(group1);
    const group2: TestGroup = new TestGroup();
    const second: TestGroupElement = new TestGroupElement(group2);

    expect(() => GroupVector.from([first, second])).toThrow(IllegalArgumentError);
  });

  test("from return same length and two vectors with elements are equal ", () => {
    const group: TestGroup = new TestGroup();
    const elements: TestGroupElement[] = [];
    const randomService: RandomService = new RandomService();
    const n: number = randomService.nextInt(100) + 1;

    for (let i = 0; i < n; i++) {
      elements[i] = new TestGroupElement(group);
    }

    const vector: GroupVector<TestGroupElement, TestGroup> = GroupVector.from(elements);
    expect(n).toEqual(vector.elements.length);

    const vector2: GroupVector<TestGroupElement, TestGroup> = GroupVector.from(elements);
    expect(vector).toEqual(vector2);
  });

  test("from with get element returns same element", () => {
    const group: TestGroup = new TestGroup();
    const elements: TestGroupElement[] = [];
    const randomService: RandomService = new RandomService();
    const n: number = randomService.nextInt(100) + 1;

    for (let i = 0; i < n; i++) {
      elements[i] = new TestGroupElement(group);
    }
    const randomIndex = randomService.nextInt(n);

    expect(elements[randomIndex]).toEqual(GroupVector.from(elements).elements[randomIndex]);
  });

  test("from with initial group equals element group", () => {
    const expectedGroup: TestGroup = new TestGroup();
    const elements: TestGroupElement[] = [];
    const randomService: RandomService = new RandomService();
    const n: number = randomService.nextInt(100) + 1;

    for (let i = 0; i < n; i++) {
      elements[i] = new TestGroupElement(expectedGroup);
    }
    expect(expectedGroup).toEqual(GroupVector.from(elements).group);
  });

  test("from with three elements with same property are all equal and different property not equal", () => {
    const group: TestGroup = new TestGroup();
    const first: TestValuedElement = new TestValuedElement(ImmutableBigInteger.ONE, group);
    const second: TestValuedElement = new TestValuedElement(ImmutableBigInteger.fromNumber(2), group);
    const third: TestValuedElement = new TestValuedElement(ImmutableBigInteger.fromNumber(3), group);

    const vector: GroupVector<TestValuedElement, TestGroup> = GroupVector.from([first, second, third]);
    expect(vector.elements.every(e => vector.elements[0].value.equals(e.value))).toBe(false);
    expect(vector.elements.every(e => vector.elements[0].group.equals(e.group))).toBe(true);
  });
});


describe("GroupVector immutability validation", () => {
  test("alter returned elements does not modify internal elements", () => {
    const group: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(47), ImmutableBigInteger.fromNumber(23), ImmutableBigInteger.fromNumber(2));
    const zqGroup = ZqGroup.sameOrderAs(group);

    const zqElement0_original = ZqElement.create(11, zqGroup);
    const groupVector: GroupVector<ZqElement, ZqGroup> = GroupVector.from([zqElement0_original]);

    expect(1).toEqual(groupVector.size);

    const elements = groupVector.elements;

    expect(elements[0].equals(zqElement0_original)).toBe(true);
    expect(elements[0].equals(groupVector.get(0))).toBe(true);
    expect(groupVector.get(0).equals(zqElement0_original)).toBe(true);

    // Alter elements MUST NOT modify groupVector
    const zqElement0_altered = ZqElement.create(7, zqGroup);
    elements[0] = zqElement0_altered;

    expect(elements[0].equals(zqElement0_altered)).toBe(true);
    expect(elements[0].equals(groupVector.get(0))).toBe(false);
    expect(groupVector.get(0).equals(zqElement0_original)).toBe(true);
  });
});