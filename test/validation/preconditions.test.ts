/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {checkArgument, checkNotNull} from "../../src/validation/preconditions";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {NullPointerError} from "../../src/error/null_pointer_error";


describe('Check argument precondition', () => {
	test("Given true as condition then not throws error", () => {
		expect(() => checkArgument(true)).not.toThrow();
	});

	test("Given false as argument then throws IllegalArgumentError", () => {
		expect(() => checkArgument(false)).toThrow(IllegalArgumentError);
	});

	test("Given null as argument then throws IllegalArgumentError", () => {
		expect(() => checkArgument(null)).toThrow(IllegalArgumentError);
	});

	test("Given undefined as argument then throws IllegalArgumentError", () => {
		expect(() => checkArgument(undefined)).toThrow(IllegalArgumentError);
	});

	test("Given false as argument and message then throws IllegalArgumentError with message", () => {
		const throwError = () => checkArgument(false, "checkArgument error message");
		expect(throwError).toThrow(new IllegalArgumentError("checkArgument error message"));
	});

	test("Given false as argument and null message then throws IllegalArgumentError with message", () => {
		const throwError = () => checkArgument(false, null);
		expect(throwError).toThrow(new IllegalArgumentError("null"));
	});

	test("Given false as argument and undefined message then throws IllegalArgumentError with message", () => {
		const throwError = () => checkArgument(false, undefined);
		expect(throwError).toThrow(new IllegalArgumentError(""));
	});
});


describe('Check not null precondition', () => {
	test("Given not null then not throws error", () => {
		expect(() => checkNotNull("")).not.toThrow();
	});

	test("Given null then throws NullPointerError", () => {
		expect(() => checkNotNull(null)).toThrow(NullPointerError);
	});

	test("Given undefined then throws NullPointerError", () => {
		expect(() => checkNotNull(undefined)).toThrow(NullPointerError);
	});

	test("Given null and message then throws NullPointerError with message", () => {
		const throwError = () => checkNotNull(null, "checkNotNull error message");
		expect(throwError).toThrow(new NullPointerError("checkNotNull error message"));
	});

	test("Given not null and null message then throws NullPointerError with message", () => {
		const throwError = () => checkNotNull(null, null);
		expect(throwError).toThrow(new NullPointerError("null"));
	});

	test("Given not null and undefined message then throws NullPointerError with message", () => {
		const throwError = () => checkNotNull(null, undefined);
		expect(throwError).toThrow(new NullPointerError(""));
	});
});