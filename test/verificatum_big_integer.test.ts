/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {VerificatumBigInteger} from "../src/verificatum_big_integer";
import {SecureRandomGenerator} from "../src/generator/secure_random_generator";
import {IllegalArgumentError} from "../src/error/illegal_argument_error";
import {ImmutableBigInteger} from "../src/immutable_big_integer";

jest.mock('vts', () => {
    // Require the original module to not be mocked...
    const originalModule = jest.requireActual('vts');

    return {
        __esModule: true, // Use it when dealing with esModules
        ...originalModule
    };
});

const DEFAULT_SIZE = 30

beforeEach(() => {
    jest.resetAllMocks();
})


describe("VerificatumBigInteger operations", () => {
    describe("static values", () => {
        test("ONE", () => {
            expect(VerificatumBigInteger.ONE.intValue()).toBe(1);
        })

        test("TEN", () => {
            expect(VerificatumBigInteger.TEN.intValue()).toBe(10);
        })
    })

    describe("constructors", () => {
        describe("case 1 -- sign and number[]", () => {
            test("12", () => {
                expect(new VerificatumBigInteger(1, Array.of(12)).intValue()).toBe(12)
            })
            test("255", () => {
                expect(new VerificatumBigInteger(1, Array.of(255)).intValue()).toBe(255)
            })
            test("256", () => {
                expect(new VerificatumBigInteger(1, Array.of(256)).intValue()).toBe(256)
            })
            test("12345678", () => {
                expect(new VerificatumBigInteger(1, Array.of(12345678)).intValue()).toBe(12345678)
            })
            test("[2,2]", () => {
                expect(new VerificatumBigInteger(1, Array.from([2, 2])).intValue()).toBe((2 ** DEFAULT_SIZE) * 2 + 2)
            })
        })

        describe("case 3 -- byte array", () => {
            test("0x0100", () => {
                let bigInteger = new VerificatumBigInteger(Array.from([0x01, 0x00]))
                expect(bigInteger.intValue()).toBe(0x0100)
            })
            test("0xffff", () => {
                let bigInteger = new VerificatumBigInteger(Array.from([0xff, 0xff]))
                expect(bigInteger.intValue()).toBe(0xffff)
            })
        })

        describe("case 4 -- hex string", () => {
            test("0x0100", () => {
                let bigInteger = new VerificatumBigInteger("0100")
                expect(bigInteger.intValue()).toBe(0x0100)
            })
            test("0xffff", () => {
                let bigInteger = new VerificatumBigInteger("ffff")
                expect(bigInteger.intValue()).toBe(0xffff)
            })
        })

        describe("case 6 -- number", () => {
            test("42", () => {
                let bigInteger = new VerificatumBigInteger(42)
                expect(bigInteger.intValue()).toBe(42)
            })

            test("longer int", () => {
                let bigInteger = new VerificatumBigInteger(2**29+43)
                expect(bigInteger.intValue()).toBe(2**29+43)
            })

            test("max safe int", () => {
                let bigInteger = new VerificatumBigInteger(Number.MAX_SAFE_INTEGER)
                expect(bigInteger.intValue()).toBe(Number.MAX_SAFE_INTEGER)
            })
        })
    })

    describe("add", () => {
        describe("1+1=2", () => {
            expect(VerificatumBigInteger.ONE.add(VerificatumBigInteger.ONE).intValue()).toBe(2)
        })
        describe("maxInt + 1 = maxInt+1", () => {
            let maxInt = new VerificatumBigInteger(Number.MAX_SAFE_INTEGER)
            expect(maxInt.add(VerificatumBigInteger.ONE).intValue()).toBe(Number.MAX_SAFE_INTEGER + 1)
        })
    })

    describe("modExp", () => {
        describe("9**2 mod 11 = 4", () => {
            let two = new VerificatumBigInteger("2")
            let nine = new VerificatumBigInteger("9")
            let eleven = new VerificatumBigInteger("b")

            let four = new VerificatumBigInteger("4")

            expect(nine.modPow(two, eleven)).toEqual(four)
        })

        describe("(-3)**(-6) mod 7 = 1", () => {
            let minusThree = new VerificatumBigInteger("3").negate();
            expect(minusThree.intValue()).toBe(-3)
            let minusSix = new VerificatumBigInteger("6").negate();
            expect(minusSix.intValue()).toBe(-6);
            let seven = new VerificatumBigInteger("7")
            expect(seven.intValue()).toBe(7)

            // negative base
            let four = minusThree.mod(seven)

            // negative exponent
            let two = four.modInverse(seven)
            let six = minusSix.negate()

            let bigInteger = two.modPow(six, seven)
            expect(bigInteger.intValue()).toBe(1)
        })

    })

    describe("toByteArray", () => {
        test("positive numbers", () => {
            expect(new VerificatumBigInteger("ff").toByteArray()).toEqual([0xff])
            expect(new VerificatumBigInteger("fffffff").toByteArray()).toEqual([0xf, 0xff, 0xff, 0xff]);
            expect(new VerificatumBigInteger("ffffffff").toByteArray()).toEqual([0xff, 0xff, 0xff, 0xff]);
        })

        test("negative numbers", () => {
            expect(() => new VerificatumBigInteger("-1").toByteArray()).toThrow(IllegalArgumentError)
        })
    })

    describe("toString", () => {
        describe("decimal", () => {
            test("from c4d955cc", () => {
                let value = 0xc4d955cc
                let bigInt = new VerificatumBigInteger(value)
                expect(bigInt.toString(10)).toEqual(value.toString(10))
            })
            test("from 357747", () => {
                let value = 357747
                let bigInt = new VerificatumBigInteger(value)
                expect(bigInt.toString(10)).toEqual(value.toString(10))
            })
            test("no leading 0 for small values", () => {
                expect(new VerificatumBigInteger(1).toString()).toBe("1")
                expect(new VerificatumBigInteger(4).toString()).toBe("4")
                expect(new VerificatumBigInteger(9).toString()).toBe("9")
            })
        })
    })

    let p3072 = new VerificatumBigInteger("B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F77BCF06C83B8FF5675F0979074AD9A787BC5B9BD4B0C5937D3EDE4C3A79396419CD7");
    describe("run some modexps", () => {
        test("sample random modexp", () => {
            let base = new VerificatumBigInteger("4")

            let exponent = ImmutableBigInteger.random(p3072.bitLength() - 1, new SecureRandomGenerator()).toBigInteger();

            expect(base.modPow(exponent, p3072)).toBeTruthy();
        })
    })
})
