/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ZqGroup} from "../../../src/math/zq_group";
import {RandomService} from "../../../src/math/random_service";
import {GqGroupGenerator} from "../gq_group_generator";
import {GqGroup} from "../../../src/math/gq_group";
import {ElGamalMultiRecipientCiphertext} from "../../../src/elgamal/elgamal_multi_recipient_ciphertext";
import {ElGamalMultiRecipientMessage} from "../../../src/elgamal/elgamal_multi_recipient_message";
import {ZqElement} from "../../../src/math/zq_element";
import {GqElement} from "../../../src/math/gq_element";
import {ElGamalMultiRecipientPublicKey} from "../../../src/elgamal/elgamal_multi_recipient_public_key";
import {checkArgument, checkNotNull} from "../../../src/validation/preconditions";
import {ImmutableBigInteger} from "../../../src/immutable_big_integer";

export class ElGamalGenerator {

	private readonly randomServiceInternal = new RandomService();

	private readonly groupInternal: GqGroup;
	private readonly groupGeneratorInternal: GqGroupGenerator;

	constructor(group: GqGroup) {
		this.groupInternal = group;
		this.groupGeneratorInternal = new GqGroupGenerator(group);
	}

	private static genPublicKey(group: GqGroup, numElements: number, randomService: RandomService): ElGamalMultiRecipientPublicKey {
		checkNotNull(randomService);
		checkNotNull(group);
		checkArgument(numElements > 0, `Cannot generate a ElGamalMultiRecipient key pair with ${numElements} elements.`);
		const N = numElements;

		const g: GqElement = group.generator;
		const privateKeyGroup = ZqGroup.sameOrderAs(group);
		const q: ImmutableBigInteger = group.q;

		// Generate the private key as a list of random exponents
		const publicKeyElements: GqElement[] = [];
		for (let i = 0; i < N; i++) {
			const sk_i = ZqElement.create(randomService.genRandomInteger(q), privateKeyGroup);
			publicKeyElements[i] = g.exponentiate(sk_i);
		}

		return new ElGamalMultiRecipientPublicKey(publicKeyElements);
	}

	public genRandomMessage(size: number): ElGamalMultiRecipientMessage {
		return new ElGamalMultiRecipientMessage(this.genRandomMessageElements(size));
	}

	public genRandomPublicKey(size: number): ElGamalMultiRecipientPublicKey {
		return ElGamalGenerator.genPublicKey(this.groupInternal, size, this.randomServiceInternal);
	}

	public genRandomCiphertext(ciphertextSize: number): ElGamalMultiRecipientCiphertext {
		const randomMessage: ElGamalMultiRecipientMessage = this.genRandomMessage(ciphertextSize);
		const randomExponent = ZqElement.create(this.randomServiceInternal.genRandomInteger(this.groupInternal.q),
			ZqGroup.sameOrderAs(this.groupInternal));

		return ElGamalMultiRecipientCiphertext.getCiphertext(randomMessage, randomExponent, this.genRandomPublicKey(ciphertextSize));
	}

	private genRandomMessageElements(size: number): GqElement[] {
		const elements: GqElement[] = [];
		elements.length = size;

		for (let i = 0; i < size; i++) {
			elements[i] = this.groupGeneratorInternal.genMember();
		}

		return elements;
	}

}