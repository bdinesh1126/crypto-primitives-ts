/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {concat, cutToBitLength} from "../src/arrays";
import {ImmutableUint8Array} from "../src/immutable_uint8Array";
import {IllegalArgumentError} from "../src/error/illegal_argument_error";
import {NullPointerError} from "../src/error/null_pointer_error";
import cutToBitLengthJson from "./cut-to-bit-length.json"
import {Base64} from "js-base64";

describe("concatenation of", () => {
	test("0 array gives empty array", () => {
		const expected = ImmutableUint8Array.from([]);

		expect(concat()).toEqual(expected);
	});

	test("one array gives same array", () => {
		const first = ImmutableUint8Array.from([1, 2]);
		const expected = ImmutableUint8Array.from([1, 2]);

		expect(concat(first)).toEqual(expected);
	});

	test("two arrays correctly gives expected concatenation", () => {
		const first = ImmutableUint8Array.from([1, 2]);
		const second = ImmutableUint8Array.from([3, 4]);
		const expected = ImmutableUint8Array.from([1, 2, 3, 4]);

		expect(concat(first, second)).toEqual(expected);
	});

	test("three arrays correctly gives expected concatenation", () => {
		const first = ImmutableUint8Array.from([1, 2]);
		const second = ImmutableUint8Array.from([3, 4]);
		const third = ImmutableUint8Array.from([5, 6]);
		const expected = ImmutableUint8Array.from([1, 2, 3, 4, 5, 6]);

		expect(concat(first, second, third)).toEqual(expected);
	});

	test("null array throws IllegalArgumentError", () => {
		expect(() => concat(null)).toThrow(new IllegalArgumentError("Arrays must not contain nulls"));
	});

	test("empty array gives empty array", () => {
		const empty = ImmutableUint8Array.from([]);
		const expected = ImmutableUint8Array.from([]);
		expect(concat(empty)).toEqual(expected);
	});
});

describe('cutToBitLength of', () => {
	test("null array throws NullPointerError", () => {
		expect(() => cutToBitLength(null, 1)).toThrow(NullPointerError);
	});

	test("null length throws IllegalArgumentError", () => {
		expect(() => cutToBitLength(ImmutableUint8Array.from([12, 24]), null)).toThrow(NullPointerError);
	});

	test("requested length 0 throws IllegalArgumentError", () => {
		expect(() => cutToBitLength(ImmutableUint8Array.from([12, 24]), 0)).toThrow(IllegalArgumentError);
	});

	test("too long requested length throws IllegalArgumentError", () => {
		expect(() => cutToBitLength(ImmutableUint8Array.from([12, 24]), 17)).toThrow(IllegalArgumentError);
	});

	test("specific input gives expected output", () => {
		const parameters = JSON.parse(JSON.stringify(cutToBitLengthJson));

		parameters.forEach((x: any) => {
			const requiredLength = x.input.bit_length;
			const byteArray: ImmutableUint8Array = ImmutableUint8Array.from(Base64.toUint8Array(x.input.value));
			const result: ImmutableUint8Array = ImmutableUint8Array.from(Base64.toUint8Array(x.output.result));

			expect(cutToBitLength(byteArray, requiredLength)).toEqual(result);
		})
	});
});