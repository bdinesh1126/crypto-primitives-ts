/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {GqElement} from "../../src/math/gq_element";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";


let p: ImmutableBigInteger;
let q: ImmutableBigInteger;
let g: ImmutableBigInteger;
let smallGroup: GqGroup;
let smallGroupGenerator: GqGroupGenerator;

beforeAll(() => {
	p = ImmutableBigInteger.fromNumber(23);
	q = ImmutableBigInteger.fromNumber(11);
	g = ImmutableBigInteger.fromNumber(2);

	smallGroup = new GqGroup(p, q, g);
	smallGroupGenerator = new GqGroupGenerator(smallGroup);
});


describe('Object instantiation validations', () => {
	test("Create group with non prime p fails", () => {
		const nonPrime: ImmutableBigInteger = ImmutableBigInteger.fromNumber(22);
		expect(() => new GqGroup(nonPrime, q, g)).toThrow(IllegalArgumentError);
	});

	test("Create group with non prime q fails", () => {
		const nonPrime: ImmutableBigInteger = ImmutableBigInteger.fromNumber(10);
		expect(() => new GqGroup(p, nonPrime, g)).toThrow(IllegalArgumentError);
	});

	test("Create group with non safe q fails", () => {
		const nonSafePrimeQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(7);
		expect(() => new GqGroup(p, nonSafePrimeQ, g)).toThrow(IllegalArgumentError);
	});

	test("Create group with non member generator fails", () => {
		const nonMember: ImmutableBigInteger = smallGroupGenerator.genNonMemberValue();
		expect(() => new GqGroup(p, q, nonMember)).toThrow(IllegalArgumentError);
	});
});


describe('Methods validations', () => {
	test("Group member returns true for group member", () => {
		const member: ImmutableBigInteger = smallGroupGenerator.genMemberValue();
		expect(smallGroup.isGroupMember(member)).toBe(true);
	});

	test("Group member returns false for non group member", () => {
		const nonMember: ImmutableBigInteger = smallGroupGenerator.genNonMemberValue();
		expect(smallGroup.isGroupMember(nonMember)).toBe(false);
	});

	test("0 is not a group member", () => {
		expect(smallGroup.isGroupMember(ImmutableBigInteger.ZERO)).toBe(false);
	});

	test("P is not a group member", () => {
		expect(smallGroup.isGroupMember(smallGroup.p)).toBe(false);
	});

	test("Null is not a group member", () => {
		expect(smallGroup.isGroupMember(null)).toBe(false);
	});

	test("Get identity element once", () => {
		const identity: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, smallGroup);
		expect(identity.equals(smallGroup.identity)).toBe(true);
	});

	test("Get identity element twice", () => {
		const identityElement: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, smallGroup);
		const firstIdentity: GqElement = smallGroup.identity;
		const secondIdentity: GqElement = smallGroup.identity;
		expect(identityElement.equals(firstIdentity)).toBe(true);
		expect(identityElement.equals(secondIdentity)).toBe(true);
	});

	test("Get q", () => {
		expect(q.equals(smallGroup.q)).toBe(true);
	});

	test("Get g", () => {
		expect(g.equals(smallGroup.generator.value)).toBe(true);
	});

	test("Equals true", () => {
		expect(new GqGroup(p, q, g).equals(smallGroup)).toBe(true);
	});
});