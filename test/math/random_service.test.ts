/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {RandomService} from "../../src/math/random_service";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ZqElement} from "../../src/math/zq_element";
import {GroupVector} from "../../src/group_vector";
import {ZqGroup} from "../../src/math/zq_group";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";


const randomService: RandomService = new RandomService();

describe('Random service validations', () => {
  test("genRandomInteger test", () => {
    const upperBound: ImmutableBigInteger = ImmutableBigInteger.fromNumber(100);
    for (let index = 0; index < 1000; index++) {
      const randomInteger: ImmutableBigInteger = randomService.genRandomInteger(upperBound);

      expect(randomInteger.compareTo(upperBound) < 0).toBe(true);
      expect(randomInteger.compareTo(ImmutableBigInteger.ZERO) >= 0).toBe(true);
    }
  });

  test("genRandomInteger with invalid upper bounds", () => {
    expect(() => randomService.genRandomInteger(null)).toThrow(NullPointerError);
    expect(() => randomService.genRandomInteger(ImmutableBigInteger.ZERO)).toThrow(IllegalArgumentError);
    const minusOne: ImmutableBigInteger = ImmutableBigInteger.ONE.negate();
    expect(() => randomService.genRandomInteger(minusOne)).toThrow(IllegalArgumentError);
  });

  test("genRandomVector test", () => {
    const upperBound: ImmutableBigInteger = ImmutableBigInteger.fromNumber(100);
    const length: number = 20;
    const randomVector: GroupVector<ZqElement, ZqGroup> = randomService.genRandomVector(upperBound, length);

    expect(length).toEqual(randomVector.size);
    expect(randomVector.elements.every(e => e.value.compareTo(upperBound) >= 0)).toBe(false);
    expect(randomVector.elements.every(e => randomVector.elements[0].group.equals(e.group))).toBe(true);
  });

  test("genRandomVector with invalid parameters", () => {
    expect(() => randomService.genRandomVector(null, 1)).toThrow(NullPointerError);
    expect(() => randomService.genRandomVector(ImmutableBigInteger.ZERO, 1)).toThrow(IllegalArgumentError);
    expect(() => randomService.genRandomVector(ImmutableBigInteger.ONE, 0)).toThrow(IllegalArgumentError);
  });

  test("nextInt test", () => {
    const upperBound: number = 100;
    for (let index = 0; index < 1000; index++) {
			const randomNumber: number = randomService.nextInt(upperBound);

      expect(randomNumber < upperBound).toBe(true);
      expect(randomNumber >= 0).toBe(true);
    }
  });

  test("nextInt with invalid upper bounds", () => {
    expect(() => randomService.nextInt(null)).toThrow(NullPointerError);
    expect(() => randomService.nextInt(0)).toThrow(IllegalArgumentError);
    expect(() => randomService.nextInt(-1)).toThrow(IllegalArgumentError);
  });

});
