/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ZqGroup} from "../../src/math/zq_group";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";


let testGroup: ZqGroup;

beforeAll(() => {
	testGroup = new ZqGroup(ImmutableBigInteger.fromNumber(10));
});


describe('Object instantiation validations', () => {
	test("Create group with null", () => {
		expect(() => new ZqGroup(null)).toThrow(NullPointerError);
	});

	test("Can not create a group with q zero", () => {
		expect(() => new ZqGroup(ImmutableBigInteger.ZERO)).toThrow(IllegalArgumentError);
	});

	test("Can not create a group with q negative", () => {
		const negativeQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(-1);
		expect(() => new ZqGroup(negativeQ)).toThrow(IllegalArgumentError);
	});
});


describe('Methods validations', () => {
	test("Null is not group member", () => {
		expect(testGroup.isGroupMember(null)).toBe(false);
	});

	test("0 is group member", () => {
		expect(testGroup.isGroupMember(ImmutableBigInteger.ZERO)).toBe(true);
	});

	test("Q is not group member", () => {
		expect(testGroup.isGroupMember(testGroup.q)).toBe(false);
	});

	test("Order and q are the same", () => {
		expect(testGroup.q.equals(testGroup.q)).toBe(true);
	});

	test("Equals validations", () => {
		const same1: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(10));
		const same2: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(10));
		const differentQ: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(20));

		expect(same1.equals(same2)).toBe(true);
		expect(same1.equals(differentQ)).toBe(false);
	});
});