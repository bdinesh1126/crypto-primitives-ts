/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {BigIntegerOperations} from "../../src/math/big_integer_operations";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";

const MINUS_ONE: ImmutableBigInteger = ImmutableBigInteger.fromNumber(-1);
const ZERO: ImmutableBigInteger = ImmutableBigInteger.ZERO;
const ONE: ImmutableBigInteger = ImmutableBigInteger.ONE;
const TWO: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
const THREE: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
const FOUR: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);
const FIVE: ImmutableBigInteger = ImmutableBigInteger.fromNumber(5);
const SIX: ImmutableBigInteger = ImmutableBigInteger.fromNumber(6);
const SEVEN: ImmutableBigInteger = ImmutableBigInteger.fromNumber(7);
const EIGHT: ImmutableBigInteger = ImmutableBigInteger.fromNumber(8);
const NINE: ImmutableBigInteger = ImmutableBigInteger.fromNumber(9);


interface Argument {
	b1: ImmutableBigInteger;
	b2: ImmutableBigInteger;
	modulus: ImmutableBigInteger;
}

function provideArguments(): Argument[] {
	return [
		{b1: null, b2: THREE, modulus: SEVEN},
		{b1: TWO, b2: null, modulus: SEVEN},
		{b1: TWO, b2: THREE, modulus: null},
	]
}

let bases: ImmutableBigInteger[];
let exponents: ImmutableBigInteger[];

beforeAll(() => {
	bases = [TWO, THREE];
	exponents = [FIVE, SIX];
});


describe("Method 'modMultiply' validations", () => {
	test("modMultiply", () => {
		expect(SIX.equals(BigIntegerOperations.modMultiply(TWO, THREE, SEVEN))).toBe(true);
		expect(SIX.equals(BigIntegerOperations.modMultiply(THREE.negate(), FIVE, SEVEN))).toBe(true);
	});

	describe('modMultiply with null parameters', () => {
		test.each(provideArguments())("n1 = $b1, n2 = $b2 and modulus = $modulus throws NullPointerError", ({b1, b2, modulus}) => {
			expect(() => BigIntegerOperations.modMultiply(b1, b2, modulus)).toThrow(NullPointerError);
		});
	});

	test("modMultiply invalid modulus", () => {
		expect(() => BigIntegerOperations.modMultiply(TWO, SIX, ONE)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modMultiply(TWO, SIX, ZERO)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modMultiply(TWO, SIX, MINUS_ONE)).toThrow(IllegalArgumentError);
	});
});


describe("Method 'modExponentiate' validations", () => {
	test("modExponentiate", () => {
		expect(ONE.equals(BigIntegerOperations.modExponentiate(TWO, THREE, SEVEN))).toBe(true);
		expect(SIX.equals(BigIntegerOperations.modExponentiate(TWO.negate(), THREE, SEVEN))).toBe(true);
		expect(ONE.equals(BigIntegerOperations.modExponentiate(TWO, THREE.negate(), SEVEN))).toBe(true);
		expect(ONE.equals(BigIntegerOperations.modExponentiate(THREE.negate(), SIX.negate(), SEVEN))).toBe(true);
	});

	describe('modExponentiate with null parameters', () => {
		test.each(provideArguments())("base = $b1, exponent = $b2 and modulus = $modulus throws NullPointerError", ({b1, b2, modulus}) => {
			expect(() => BigIntegerOperations.modMultiply(b1, b2, modulus)).toThrow(NullPointerError);
		});
	});

	test("modExponentiate invalid modulus", () => {
		expect(() => BigIntegerOperations.modExponentiate(TWO, SIX, ONE)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modExponentiate(TWO, SIX, ZERO)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modExponentiate(TWO, SIX, MINUS_ONE)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modExponentiate(TWO, SIX, TWO)).toThrow(IllegalArgumentError);
	});

	test("modExponentiate base modulus not relatively prime", () => {
		expect(EIGHT.equals(BigIntegerOperations.modExponentiate(TWO, THREE, NINE))).toBe(true);
		const negativeThree: ImmutableBigInteger = THREE.negate()
		expect(() => BigIntegerOperations.modExponentiate(THREE, negativeThree, NINE)).toThrow(IllegalArgumentError);
	});
});

describe("Method 'multiModExp' validations", () => {
	test("multiModExp", () => {
		const basesOneNegative = [TWO, THREE.negate()];
		const exponentsOneNegative = [FIVE, SIX.negate()];

		expect(FOUR.equals(BigIntegerOperations.multiModExp(bases, exponents, SEVEN))).toBe(true);
		expect(FOUR.equals(BigIntegerOperations.multiModExp(basesOneNegative, exponents, SEVEN))).toBe(true);
		expect(FOUR.equals(BigIntegerOperations.multiModExp(bases, exponentsOneNegative, SEVEN))).toBe(true);
		expect(FOUR.equals(BigIntegerOperations.multiModExp(basesOneNegative, exponentsOneNegative, SEVEN))).toBe(true);
	});

	test("multiModExp null arguments", () => {
		expect(() => BigIntegerOperations.multiModExp(null, exponents, SEVEN)).toThrow(NullPointerError);
		expect(() => BigIntegerOperations.multiModExp(bases, null, SEVEN)).toThrow(NullPointerError);
		expect(() => BigIntegerOperations.multiModExp(bases, exponents, null)).toThrow(NullPointerError);
	});

	test("multiModExp invalid modulus", () => {
		expect(() => BigIntegerOperations.multiModExp(bases, exponents, ONE)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.multiModExp(bases, exponents, ZERO)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.multiModExp(bases, exponents, MINUS_ONE)).toThrow(IllegalArgumentError);
	});

	test("multiModExp bases modulus not relatively prime", () => {
		expect(ZERO.equals(BigIntegerOperations.multiModExp(bases, exponents, NINE))).toBe(true);
		const exponentsOneNegative = [FIVE, SIX.negate()];
		expect(() => BigIntegerOperations.multiModExp(bases, exponentsOneNegative, NINE)).toThrow(IllegalArgumentError);
	});

	test("multiModExp empty bases", () => {
		const emptyBases: ImmutableBigInteger[] = [];
		expect(() => BigIntegerOperations.multiModExp(emptyBases, exponents, SEVEN)).toThrow(IllegalArgumentError);
	});

	test("multiModExp empty exponents", () => {
		const emptyExponents: ImmutableBigInteger[] = [];
		expect(() => BigIntegerOperations.multiModExp(bases, emptyExponents, SEVEN)).toThrow(IllegalArgumentError);
	});

	test("multiModExp bases different size exponents", () => {
		expect(() => BigIntegerOperations.multiModExp([...bases, FIVE], exponents, SEVEN)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.multiModExp(bases, [...exponents, FIVE], SEVEN)).toThrow(IllegalArgumentError);
	});
});

describe("Method 'modInvert' validations", () => {
	test("modInvert", () => {
		expect(ONE.equals(BigIntegerOperations.modInvert(ONE, SEVEN))).toBe(true);
		expect(FIVE.equals(BigIntegerOperations.modInvert(THREE, SEVEN))).toBe(true);
	})

	test("modInvert null arguments", () => {
		expect(() => BigIntegerOperations.modInvert(null, SEVEN)).toThrow(NullPointerError);
		expect(() => BigIntegerOperations.modInvert(ONE, null)).toThrow(NullPointerError);
	})

	test("modInvert invalid modulus", () => {
		expect(() => BigIntegerOperations.modInvert(TWO, ONE)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modInvert(TWO, ZERO)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modInvert(TWO, MINUS_ONE)).toThrow(IllegalArgumentError);
	})

	test("modInvert non invertible element", () => {
		expect(() => BigIntegerOperations.modInvert(TWO, SIX)).toThrow(IllegalArgumentError);
		expect(() => BigIntegerOperations.modInvert(THREE, SIX)).toThrow(IllegalArgumentError);
	})
})