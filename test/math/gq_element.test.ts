/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {GqElement} from "../../src/math/gq_element";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {ZqGroup} from "../../src/math/zq_group";
import {ZqElement} from "../../src/math/zq_element";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";


let g: ImmutableBigInteger;
let group: GqGroup;
let groupGenerator: GqGroupGenerator;

beforeAll(() => {
	const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
	const q: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
	g = ImmutableBigInteger.fromNumber(2);

	group = new GqGroup(p, q, g);
	groupGenerator = new GqGroupGenerator(group);
});


describe('GqElement fromValue validation', () => {
	test("Given a value when a group element is created with that value then has that value", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const element: GqElement = GqElement.fromValue(value, group);
		expect(value.equals(element.value)).toBe(true);
	});

	test("When create an element with value zero then error", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		expect(() => GqElement.fromValue(value, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element with negative value then error", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(-1);
		expect(() => GqElement.fromValue(value, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element with value greater than p then error", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(24);
		expect(() => GqElement.fromValue(value, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element with null value then error", () => {
		expect(() => GqElement.fromValue(null, group)).toThrow(NullPointerError);
	});

	test("When create an element with null group then error", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.ONE;
		expect(() => GqElement.fromValue(value, null)).toThrow(NullPointerError);
	});

	test("When create an element not member of the group then error", () => {
		const nonMemberValue: ImmutableBigInteger = groupGenerator.genNonMemberValue();
		expect(() => GqElement.fromValue(nonMemberValue, group)).toThrow(IllegalArgumentError);
	});

	test("When create an element member of the group no error", () => {
		const memberValue: ImmutableBigInteger = groupGenerator.genMemberValue();
		const groupMember: GqElement = GqElement.fromValue(memberValue, group);
		expect(group.isGroupMember(groupMember.value)).toBe(true);
	});
});

describe('GqElement fromSquareRoot validation', () => {
	test("with null values throws a NullPointerError ", () => {
		expect(() => GqElement.fromSquareRoot(null, group)).toThrow(NullPointerError);
		expect(() => GqElement.fromSquareRoot(ImmutableBigInteger.ONE, null)).toThrow(NullPointerError);
	});

	test("with zero throws an IllegalArgumentError", () => {
		expect(() => GqElement.fromSquareRoot(ImmutableBigInteger.ZERO, group)).toThrow(
			new IllegalArgumentError("The element must be strictly greater than 0")
		);
	});

	test("with too big element throws an IllegalArgumentError", () => {
		expect(() => GqElement.fromSquareRoot(group.q, group)).toThrow(
			new IllegalArgumentError("The element must be smaller than the group's order")
		);
	});

	test("with valid inputs returns squared element", () => {
		const two = ImmutableBigInteger.fromNumber(2);
		const five = ImmutableBigInteger.fromNumber(5);

		const resultOne: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, group);
		const resultFour: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(4), group);
		const resultTwo: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(2), group);

		expect(GqElement.fromSquareRoot(ImmutableBigInteger.ONE, group).equals(resultOne)).toBe(true);
		expect(GqElement.fromSquareRoot(two, group).equals(resultFour)).toBe(true);
		expect(GqElement.fromSquareRoot(five, group).equals(resultTwo)).toBe(true);
	});

});

describe('Inversion method validations', () => {
	test("Given an element when inverted then succeeds", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(16);
		const expectedInverseValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(13);
		invertAndAssert(value, expectedInverseValue);
	});

	test("Given an element with value one when inverted then result is one", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.ONE;
		const expectedInverseValue: ImmutableBigInteger = ImmutableBigInteger.ONE;
		invertAndAssert(value, expectedInverseValue);
	});
});


describe('Multiplication method validations', () => {
	test("Given null element when multiply then exception", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const element1: GqElement = GqElement.fromValue(value1, group);
		expect(() => element1.multiply(null)).toThrow(NullPointerError);
	});

	test("Given two elements from different groups when multiply then exception", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const value2: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

		const element1: GqElement = GqElement.fromValue(value1, group);
		const element2: GqElement = GqElement.fromValue(value2, new GqGroup(ImmutableBigInteger.fromNumber(7), ImmutableBigInteger.fromNumber(3), g));

		expect(() => element1.multiply(element2)).toThrow(IllegalArgumentError);
	});

	test("Given two elements when multiplied then succeeds", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const value2: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(12);

		multiplyAndAssert(value1, value2, expectedResult);
	});

	test("Given an element with value one when multiplied with a second element then the result is second element", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const value2: ImmutableBigInteger = ImmutableBigInteger.ONE;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

		multiplyAndAssert(value1, value2, expectedResult);
	});

	test("Given two element when multiplied then the result is smaller than p", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(12);
		const value2: ImmutableBigInteger = ImmutableBigInteger.fromNumber(13);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(18);

		multiplyAndAssert(value1, value2, expectedResult);
	});

});


describe('Exponentiation method validations', () => {
	test("Given element and null exponent when exponentiate then exception", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const element: GqElement = GqElement.fromValue(value1, group);

		expect(() => element.exponentiate(null)).toThrow(NullPointerError);
	});

	test("Given element and exponent from different groups when exponentiate then exception", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const element: GqElement = GqElement.fromValue(value1, group);

		const exponentGroup: ZqGroup = new ZqGroup(ImmutableBigInteger.fromNumber(3));
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponent: ZqElement = ZqElement.create(exponentValue, exponentGroup);

		expect(() => element.exponentiate(exponent)).toThrow(IllegalArgumentError);
	});

	test("Given an exponent with value zero when exponentiate with it then result is one", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(16);
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.ONE;

		exponentiateAndAssert(value, exponentValue, expectedResult);
	});

	test("Given element and exponent when exponentiate then succeeds", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(16);

		exponentiateAndAssert(value, exponentValue, expectedResult);
	});

	test("Given element and exponent when exponentiation then result greater than q", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(13);
		const exponentValue: ImmutableBigInteger = ImmutableBigInteger.fromNumber(5);
		const expectedResult: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);

		exponentiateAndAssert(value, exponentValue, expectedResult);
	});

	test("Test exponentiate with a null element", () => {
		const element: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, group);

		expect(() => element.exponentiate(null)).toThrow(NullPointerError);
	});

	test("Test invert one equals one", () => {
		const one: GqElement = group.identity;
		const inverse = one.invert();
		expect(one.equals(inverse)).toBe(true);
	});

	test("Test invert", () => {
		for (let i = 1; i < group.q.intValue(); i++) {
			const exponent: ZqElement = ZqElement.create(i, ZqGroup.sameOrderAs(group));
			const element: GqElement = group.generator.exponentiate(exponent);
			expect(group.identity.equals(element.multiply(element.invert()))).toBe(true);
		}
	});

});

describe("Divide method validations", () => {
	test("Given element and null divisor when divide then exception", () => {
		let element = GqElement.fromValue(ImmutableBigInteger.fromNumber(1), group);
		expect(() => element.divide(null)).toThrow(NullPointerError);
	});

	test("Given element and divisor from different groups when divide then exception", () => {
		const value1: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const value2: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

		const element1: GqElement = GqElement.fromValue(value1, group);
		const element2: GqElement = GqElement.fromValue(value2, new GqGroup(ImmutableBigInteger.fromNumber(7), ImmutableBigInteger.fromNumber(3), g));

		expect(() => element1.divide(element2)).toThrow(IllegalArgumentError);
	});

	test("Divide element by itself gives 1", () => {
		let gqElements = groupGenerator.genRandomGqElementVector(10);
		gqElements.elements.forEach(e => expect(e.divide(e)).toEqual(group.identity));
	});

	test("Given two elements when multiplied then succeeds", () => {
		const integer2: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
		const integer3: ImmutableBigInteger = ImmutableBigInteger.fromNumber(3);
		const integer4: ImmutableBigInteger = ImmutableBigInteger.fromNumber(4);
		const integer9: ImmutableBigInteger = ImmutableBigInteger.fromNumber(9);
		const integer12: ImmutableBigInteger = ImmutableBigInteger.fromNumber(12);

		const two: GqElement = GqElement.fromValue(integer2, group);
		const three: GqElement = GqElement.fromValue(integer3, group);
		const four: GqElement = GqElement.fromValue(integer4, group);
		const nine: GqElement = GqElement.fromValue(integer9, group);
		const twelve: GqElement = GqElement.fromValue(integer12, group);

		expect(four.divide(two).equals(two)).toBe(true);
		expect(twelve.divide(three).equals(four)).toBe(true);
		expect(four.divide(three).equals(nine)).toBe(true);
	});
});


test("Equals method validations", () => {
	const element1_value1_q11: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, group);
	const element2_value1_q11: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, group);

	const element3_value2_q11: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(2), group);

	const otherGroup_g2_q3: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(7), ImmutableBigInteger.fromNumber(3), ImmutableBigInteger.fromNumber(2));
	const element4_value1_q3: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, otherGroup_g2_q3);

	expect(element1_value1_q11.equals(element2_value1_q11)).toBe(true);
	expect(!element1_value1_q11.equals(element3_value2_q11)).toBe(true);
	expect(!element1_value1_q11.equals(element4_value1_q3)).toBe(true);
	expect(!element3_value2_q11.equals(element4_value1_q3)).toBe(true);
});


/**
 * Multiplies two group elements with the values <code>value1</code> and <code>value2</code>. Then asserts that the result has the value
 * <code>expectedResult</code>.
 *
 * @param value1         First element to multiply.
 * @param value2         Second element to multiply.
 * @param expectedResult The expected result of the <code>value1 * value2</code>.
 */
function multiplyAndAssert(value1: ImmutableBigInteger, value2: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {
	const element1: GqElement = GqElement.fromValue(value1, group);
	const element2: GqElement = GqElement.fromValue(value2, group);
	const result: GqElement = element1.multiply(element2);
	expect(expectedResult.equals(result.value)).toBe(true);
}

/**
 * Inverts the element with the value <code>elementValue</code>, and checks whether the result is the <code>expectedInverseValue</code>.
 *
 * @param elementValue         The value of the element to invert.
 * @param expectedInverseValue The expected result of the invert operation of the element with value <code>elementValue</code>.
 */
function invertAndAssert(elementValue: ImmutableBigInteger, expectedInverseValue: ImmutableBigInteger) {
	const element: GqElement = GqElement.fromValue(elementValue, group);
	const inverse: GqElement = element.invert();
	expect(expectedInverseValue.equals(inverse.value)).toBe(true);
}

/**
 * Exponentiates an element by an exponent and asserts the expected result.
 *
 * @param elementValue   The group element value to set.
 * @param exponentValue  The exponent value to set.
 * @param expectedResult The expected result of the exponentiation.
 */
function exponentiateAndAssert(elementValue: ImmutableBigInteger, exponentValue: ImmutableBigInteger, expectedResult: ImmutableBigInteger) {
	const element: GqElement = GqElement.fromValue(elementValue, group);
	const exponent: ZqElement = ZqElement.create(exponentValue, ZqGroup.sameOrderAs(group));
	const result: GqElement = element.exponentiate(exponent);
	expect(expectedResult.equals(result.value)).toBe(true);
}