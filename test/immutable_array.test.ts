/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Hashable} from "../src/hashing/hashable";
import {ImmutableArray} from "../src/immutable_array";
import {ImmutableBigInteger} from "../src/immutable_big_integer";

test("pushing to original array does not modify immutable array", () => {
	const array: Hashable[] = ["12", "34"];
	const immutableArray = ImmutableArray.from(array);

	array.push("56");

	expect(immutableArray.length).toBe(2);
	expect(immutableArray.get(0)).toBe("12");
	expect(immutableArray.get(1)).toBe("34");
});

test("changing original array value does not modify immutable array", () => {
	const array: Hashable[] = ["12", "34"];
	const immutableArray = ImmutableArray.from(array);

	array[0] = "56";

	expect(immutableArray.length).toBe(2);
	expect(immutableArray.get(0)).toBe("12");
	expect(immutableArray.get(1)).toBe("34");
});

test("using map on immutable array does not modify immutable array", () => {
	const array: Hashable[] = ["12", "34"];
	const immutableArray = ImmutableArray.from(array);

	immutableArray.map(_ => "0");

	expect(immutableArray.length).toBe(2);
	expect(immutableArray.get(0)).toBe("12");
	expect(immutableArray.get(1)).toBe("34");
});

test("using map on original array does not modify immutable array", () => {
	const a = ImmutableBigInteger.fromNumber(12);
	const b = ImmutableBigInteger.fromNumber(34);
	const array: ImmutableBigInteger[] = [a, b];
	const immutableArray = ImmutableArray.from(array);

	array.map(b => b.add(ImmutableBigInteger.ONE));

	expect(immutableArray.length).toBe(2);
	expect(immutableArray.get(0)).toEqual(a);
	expect(immutableArray.get(1)).toEqual(b);
});
