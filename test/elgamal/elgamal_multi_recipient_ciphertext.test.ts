/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {ElGamalMultiRecipientCiphertext} from "../../src/elgamal/elgamal_multi_recipient_ciphertext";
import {ElGamalMultiRecipientPublicKey} from "../../src/elgamal/elgamal_multi_recipient_public_key";
import {ElGamalMultiRecipientMessage} from "../../src/elgamal/elgamal_multi_recipient_message";
import {ZqElement} from "../../src/math/zq_element";
import {RandomService} from "../../src/math/random_service";
import {ZqGroup} from "../../src/math/zq_group";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {ElGamalGenerator} from "../tools/generators/elgamal_generator";

let gqGroup: GqGroup;
let generator: GqGroupGenerator;
const randomService: RandomService = new RandomService();

const NUM_RECIPIENTS: number = 10;
const PUBLIC_KEY_SIZE: number = 10;

function initializeTest() {
	const p: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
	const q: ImmutableBigInteger = ImmutableBigInteger.fromNumber(11);
	const g: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);

	gqGroup = new GqGroup(p, q, g);
	generator = new GqGroupGenerator(gqGroup);
}

beforeEach(() => {
	initializeTest();
});

describe("Get a ciphertext", () => {
	describe('with invalid parameters', () => {
		interface InvalidArgument {
			message: ElGamalMultiRecipientMessage;
			exponent: ZqElement;
			publicKey: ElGamalMultiRecipientPublicKey
			exception: Error;
		}

		function createInvalidArgumentsProvider(): InvalidArgument[] {

			initializeTest();
			// Valid parameters
			const validMessage: ElGamalMultiRecipientMessage = generateRandomMessage();
			const zqGroup: ZqGroup = ZqGroup.sameOrderAs(gqGroup);
			const validExponent: ZqElement = ZqElement.create(randomService.genRandomInteger(zqGroup.q), zqGroup);
			const validPK: ElGamalMultiRecipientPublicKey = generateRandomPublicKey(randomService.nextInt(PUBLIC_KEY_SIZE) + 1);

			// Invalid parameters
			const otherP: ImmutableBigInteger = ImmutableBigInteger.fromNumber(47);
			const otherQ: ImmutableBigInteger = ImmutableBigInteger.fromNumber(23);
			const otherG: ImmutableBigInteger = ImmutableBigInteger.fromNumber(2);
			const otherGroup: GqGroup = new GqGroup(otherP, otherQ, otherG);
			const otherZqGroup: ZqGroup = ZqGroup.sameOrderAs(otherGroup);
			const otherGroupExponent: ZqElement = ZqElement.create(randomService.genRandomInteger(otherZqGroup.q), otherZqGroup);
			const otherGroupPublicKey: ElGamalMultiRecipientPublicKey = generateRandomPublicKey(randomService.nextInt(PUBLIC_KEY_SIZE) + 1, otherGroup);
			const tooShortPK: ElGamalMultiRecipientPublicKey = generateRandomPublicKey(NUM_RECIPIENTS - 1);

			return [
				{message: null, exponent: validExponent, publicKey: validPK, exception: new NullPointerError()},
				{message: validMessage, exponent: null, publicKey: validPK, exception: new NullPointerError()},
				{message: validMessage, exponent: validExponent, publicKey: null, exception: new NullPointerError()},
				{
					message: validMessage,
					exponent: otherGroupExponent,
					publicKey: validPK,
					exception: new IllegalArgumentError("Exponent and message groups must be of the same order.")
				},
				{
					message: validMessage,
					exponent: validExponent,
					publicKey: otherGroupPublicKey,
					exception: new IllegalArgumentError("Message and public key must belong to the same group.")
				},
				{
					message: validMessage,
					exponent: validExponent,
					publicKey: tooShortPK,
					exception: new IllegalArgumentError("There cannot be more message elements than public key elements.")
				}
			]
		}

		test.each(createInvalidArgumentsProvider())("throws $exception", ({message, exponent, publicKey, exception}) => {
			expect(() => ElGamalMultiRecipientCiphertext.getCiphertext(message, exponent, publicKey)).toThrow(exception);
		});
	});
});

describe("Get a ciphertext", () => {
	let validPK: ElGamalMultiRecipientPublicKey;
	beforeEach(() => {
		validPK = generateRandomPublicKey(PUBLIC_KEY_SIZE);
	});

	test("when identity randomness and identity message elements then gamma is generator and ciphertext is publicKey", () => {
		const zqGroup: ZqGroup = ZqGroup.sameOrderAs(gqGroup);
		const one: ZqElement = ZqElement.create(ImmutableBigInteger.ONE, zqGroup);
		const onesMessage: ElGamalMultiRecipientMessage = generateMessage(gqGroup.identity);

		const ciphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(onesMessage, one, validPK);

		expect(gqGroup.generator.equals(ciphertext.gamma)).toBe(true);
		expect(validPK.stream().every((keyElement, index) => keyElement.equals(ciphertext.get(index)))).toBe(true);
	});

	test("fewer messages than keys with identity randomness and identity message elements", () => {
		const nMessages: number = NUM_RECIPIENTS / 2;
		const zqGroup: ZqGroup = ZqGroup.sameOrderAs(gqGroup);
		const oneExponent: ZqElement = ZqElement.create(ImmutableBigInteger.ONE, zqGroup);
		const smallOneMessage: ElGamalMultiRecipientMessage = generateMessage(GqElement.fromValue(ImmutableBigInteger.ONE, gqGroup), nMessages);
		const ciphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(smallOneMessage, oneExponent, validPK);

		// With a exponent of one and message of ones, the ciphertext phis is just the public key
		const keyElements = validPK.stream().slice(0, nMessages - 1); // limit to nMessages -1
		const phisElements = ciphertext.stream().slice(1); // skip gamma
		expect(keyElements.every((keyElement, index) => keyElement.equals(phisElements[index]))).toBe(true);
	});

	test("when zero exponent gives message", () => {
		const zqGroup: ZqGroup = ZqGroup.sameOrderAs(gqGroup);
		const zeroExponent: ZqElement = ZqElement.create(ImmutableBigInteger.ZERO, zqGroup);
		const validMessage: ElGamalMultiRecipientMessage = generateRandomMessage();
		const ciphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(validMessage, zeroExponent, validPK);

		const messageElements = validMessage.stream();
		const phisElements = ciphertext.stream().slice(1); // skip gamma
		expect(messageElements.every((messageElement, index) => messageElement.equals(phisElements[index]))).toBe(true);

		expect(gqGroup.identity.equals(ciphertext.gamma)).toBe(true);
	});

	test("with specific values", () => {
		const group: GqGroup = new GqGroup(ImmutableBigInteger.fromNumber(11), ImmutableBigInteger.fromNumber(5), ImmutableBigInteger.fromNumber(3));
		const message: ElGamalMultiRecipientMessage =
			new ElGamalMultiRecipientMessage([
				GqElement.fromValue(ImmutableBigInteger.fromNumber(4), group),
				GqElement.fromValue(ImmutableBigInteger.fromNumber(5), group)
			]);
		const exponent: ZqElement = ZqElement.create(ImmutableBigInteger.fromNumber(2), ZqGroup.sameOrderAs(group));
		const publicKey: ElGamalMultiRecipientPublicKey =
			new ElGamalMultiRecipientPublicKey([
				GqElement.fromValue(ImmutableBigInteger.fromNumber(5), group),
				GqElement.fromValue(ImmutableBigInteger.fromNumber(9), group)
			]);
		const ciphertext: ElGamalMultiRecipientCiphertext =
			ElGamalMultiRecipientCiphertext.create(
				GqElement.fromValue(ImmutableBigInteger.fromNumber(9), group),
				[GqElement.fromValue(ImmutableBigInteger.ONE, group),
					GqElement.fromValue(ImmutableBigInteger.fromNumber(9), group)]
			);

		expect(ciphertext.equals(ElGamalMultiRecipientCiphertext.getCiphertext(message, exponent, publicKey))).toBe(true);
	});
});


describe("Exponentiate a ciphertext", () => {
	test("with null argument throws", () => {
		const ciphertext: ElGamalMultiRecipientCiphertext = new ElGamalGenerator(gqGroup).genRandomCiphertext(5);

		expect(() => ciphertext.getCiphertextExponentiation(null)).toThrow(NullPointerError);
	});

	test("with exponent of different group size throws", () => {
		const ciphertext: ElGamalMultiRecipientCiphertext = new ElGamalGenerator(gqGroup).genRandomCiphertext(5);
		const exponentDifferentGroup: ZqElement = ZqElement.create(1, new ZqGroup(gqGroup.q.add(ImmutableBigInteger.ONE)));

		expect(() => ciphertext.getCiphertextExponentiation(exponentDifferentGroup)).toThrow(IllegalArgumentError);
	});

	test("with specific values returns expected result", () => {
		const zqGroup: ZqGroup = ZqGroup.sameOrderAs(gqGroup);
		const zZero: ZqElement = ZqElement.create(0, zqGroup);
		const zOne: ZqElement = ZqElement.create(1, zqGroup);
		const zTwo: ZqElement = ZqElement.create(2, zqGroup);

		const gOne: GqElement = GqElement.fromValue(ImmutableBigInteger.ONE, gqGroup);
		const gFour: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(4), gqGroup);
		const gEight: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(8), gqGroup);
		const gThirteen: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(13), gqGroup);
		const gSixteen: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(16), gqGroup);
		const gEighteen: GqElement = GqElement.fromValue(ImmutableBigInteger.fromNumber(18), gqGroup);

		const ciphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(gThirteen, [gFour, gEight]);

		const oneCiphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(gOne, [gOne, gOne]);
		const squaredCiphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(gEight, [gSixteen, gEighteen]);

		expect(ciphertext.getCiphertextExponentiation(zZero).equals(oneCiphertext)).toBe(true);
		expect(ciphertext.getCiphertextExponentiation(zOne).equals(ciphertext)).toBe(true);
		expect(ciphertext.getCiphertextExponentiation(zTwo).equals(squaredCiphertext)).toBe(true);
	});
});


function generateRandomMessage(): ElGamalMultiRecipientMessage {
	const messageElements: GqElement[] = [];
	for (let i = 0; i < NUM_RECIPIENTS; i++) {
		messageElements.push(generator.genMember());
	}
	return new ElGamalMultiRecipientMessage(messageElements);
}

function generateMessage(element: GqElement, nMessages?: number): ElGamalMultiRecipientMessage {
	let numberOfMessages = NUM_RECIPIENTS;
	if (nMessages) {
		numberOfMessages = nMessages;
	}
	const messageElements: GqElement[] = [];
	for (let i = 0; i < numberOfMessages; i++) {
		messageElements.push(element);
	}
	return new ElGamalMultiRecipientMessage(messageElements);
}

function generateRandomPublicKey(length: number, gqGroup?: GqGroup): ElGamalMultiRecipientPublicKey {
	let generatorInternal: GqGroupGenerator = generator;
	if (gqGroup) {
		generatorInternal = new GqGroupGenerator(gqGroup);
	}
	const keyElements: GqElement[] = [];
	for (let i = 0; i < length; i++) {
		keyElements.push(generatorInternal.genMember());
	}
	return new ElGamalMultiRecipientPublicKey(keyElements);
}
