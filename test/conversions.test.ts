/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {byteArrayToInteger, integerToByteArray, integerToString, stringToByteArray, stringToInteger} from "../src/conversions";
import {NullPointerError} from "../src/error/null_pointer_error";
import {ImmutableUint8Array} from "../src/immutable_uint8Array";
import {IllegalArgumentError} from "../src/error/illegal_argument_error";
import {SecureRandomGenerator} from "../src/generator/secure_random_generator";
import {RandomService} from "../src/math/random_service";
import {ImmutableBigInteger} from "../src/immutable_big_integer";

const secureRandomGenerator = new SecureRandomGenerator();

describe("byteArrayToInteger of", () => {
	test("null byte array to biginteger throws", () => {
		expect(() => byteArrayToInteger(null)).toThrow(NullPointerError)
	});

	test("empty byte array to biginteger throws", () => {
		expect(() => byteArrayToInteger(ImmutableUint8Array.from([]))).toThrow(IllegalArgumentError);
	});

	test("byte array with leading 1 to biginteger is positive", () => {
		const bytes: ImmutableUint8Array = ImmutableUint8Array.from([0x80]);
		const converted: ImmutableBigInteger = byteArrayToInteger(bytes);
		expect(converted.compareTo(ImmutableBigInteger.ZERO) > 0).toBe(true);
	});

	test("256 byte array representation is 256", () => {
		const bytes: ImmutableUint8Array = ImmutableUint8Array.from([1, 0]);
		const converted: ImmutableBigInteger = byteArrayToInteger(bytes);
		expect(converted.compareTo(ImmutableBigInteger.fromNumber(256)) === 0).toBe(true);
	});

	test("random biginteger to byte and back is original value", () => {
		const randomService = new RandomService();
		for (let i = 0; i < 100; i++) {
			const size = randomService.nextInt(32) + 1; // size needs to be > 0
			const value: ImmutableBigInteger = ImmutableBigInteger.random(size, secureRandomGenerator);
			const cycledValue: ImmutableBigInteger = byteArrayToInteger(integerToByteArray(value));

			expect(cycledValue.equals(value)).toBe(true);
		}
	});
});

describe("integerToByteArray of", () => {
	test("null biginteger to byte array throws", () => {
		expect(() => integerToByteArray(null)).toThrow(NullPointerError)
	});

	test("zero biginteger is one zero byte", () => {
		const zero: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const expected: ImmutableUint8Array = ImmutableUint8Array.from([0]);
		const converted: ImmutableUint8Array = integerToByteArray(zero);

		expect(expected).toEqual<ImmutableUint8Array>(converted);
	});

	test("256 biginteger is two bytes", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(256);
		const expected: ImmutableUint8Array = ImmutableUint8Array.from([1, 0])
		const converted: ImmutableUint8Array = integerToByteArray(value);

		expect(expected).toEqual<ImmutableUint8Array>(converted);
	});

	test("integer max value plus one is correct", () => {
		let maxSafeInt = ImmutableBigInteger.fromNumber(Number.MAX_SAFE_INTEGER);
		expect(maxSafeInt.intValue()).toBe(Number.MAX_SAFE_INTEGER)
		expect(maxSafeInt.toByteArray()).toEqual(ImmutableUint8Array.from([0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff]).toArray())
		const value: ImmutableBigInteger = maxSafeInt.add(ImmutableBigInteger.ONE);
		const expected: ImmutableUint8Array = ImmutableUint8Array.from([0x20, 0, 0, 0, 0, 0, 0]);
		const converted: ImmutableUint8Array = integerToByteArray(value);

		expect(expected).toEqual<ImmutableUint8Array>(converted);
	});

	test("negative biginteger throws", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.fromNumber(-1);

		expect(() => integerToByteArray(value)).toThrow(IllegalArgumentError);
	});
});

describe("stringToByteArray of", () => {
	test("null string to byte array throws", () => {
		expect(() => stringToByteArray(null)).toThrow(NullPointerError);
	});

	test("invalid UTF-8 string to byte array throws", () => {
		expect(() => stringToByteArray("\uD8E5")).toThrow(IllegalArgumentError);
		expect(() => stringToByteArray("All good until here \uDFFF")).toThrow(IllegalArgumentError);
		expect(() => stringToByteArray("\uDEEF and something else")).toThrow(IllegalArgumentError);
	});

	test("string to byte array is correct", () => {
		const expected: ImmutableUint8Array = ImmutableUint8Array.from([226, 130, 172]);
		const converted: ImmutableUint8Array = stringToByteArray("€");

		expect(expected).toEqual<ImmutableUint8Array>(converted);
	});
});

describe("stringToInteger of", () => {
	test("null string to biginteger throws", () => {
		expect(() => stringToInteger(null)).toThrow(NullPointerError)
	});

	test("empty string to biginteger throws", () => {
		expect(() => stringToInteger("")).toThrow(new IllegalArgumentError("The string to convert cannot be empty."));
	});

	test("alphanumeric string to biginteger throws", () => {
		const expectedError = new IllegalArgumentError("The string to convert is not a valid decimal representation of an ImmutableBigInteger.");
		expect(() => stringToInteger("1A")).toThrow(expectedError);
		expect(() => stringToInteger("A1")).toThrow(expectedError);
	});

	test("non decimal string to biginteger throws", () => {
		const expectedError = new IllegalArgumentError("The string to convert is not a valid decimal representation of an ImmutableBigInteger.");
		expect(() => stringToInteger("-1")).toThrow(expectedError);
	});

	test("numeric string to biginteger is correct", () => {

		expect(stringToInteger("0").equals(ImmutableBigInteger.ZERO)).toBe(true);
		expect(stringToInteger("1").equals(ImmutableBigInteger.ONE)).toBe(true);
		expect(stringToInteger("1001").equals(ImmutableBigInteger.fromNumber(1001))).toBe(true);
	});

	test("numeric string with leading zeros to biginteger is correct", () => {
		expect(stringToInteger("0021").equals(ImmutableBigInteger.fromNumber(21))).toBe(true);
	});
});

describe("integerToString of", () => {
	test("null biginteger to string throws", () => {
		expect(() => integerToString(null)).toThrow(NullPointerError)
	});

	test("negative bigInteger to string throws", () => {
		expect(() => integerToString(ImmutableBigInteger.fromNumber(-1))).toThrow(new IllegalArgumentError());
	});

	test("zero biginteger to string and back is original value", () => {
		const value: ImmutableBigInteger = ImmutableBigInteger.ZERO;
		const cycledValue: ImmutableBigInteger = stringToInteger(integerToString(value));

		expect(cycledValue.equals(value)).toBe(true);
	});

	test("random biginteger to string and back is original value", () => {
		expect.extend({
			toBeTruthyWithMessage(received: any, errMsg: string) {
				const result = {
					pass: received,
					message: () => errMsg
				};
				return result;
			}
		});

		const randomService = new RandomService();
		for (let i = 0; i < 100; i++) {
			const size = randomService.nextInt(32) + 1; // Size needs to be > 0
			const value: ImmutableBigInteger = ImmutableBigInteger.random(size, secureRandomGenerator);
			const cycledValue: ImmutableBigInteger = stringToInteger(integerToString(value));

			expect(cycledValue.equals(value)).toBeTruthy();
		}
	});
});
