/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ShakeDigest} from "../../src/hashing/shake_digest";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";

const data: ImmutableUint8Array = ImmutableUint8Array.from([1, 2, 3]);
let shakeDigest: ShakeDigest;

beforeAll(() => {
	shakeDigest = new ShakeDigest();
})

describe("digest", () => {
	test("with null data throws", () => {
		expect(() => shakeDigest.digest(null, 1)).toThrow(NullPointerError);
	});

	test("with null output length to throw", () => {
		expect(() => shakeDigest.digest(data, null)).toThrow(NullPointerError);
	});

	test("with output length smaller than 1 to throw", () => {
		expect(() => shakeDigest.digest(data, 0)).toThrow(new IllegalArgumentError("The output length must be strictly positive"));
	});

	test("with valid input not to throw", () => {
		expect(() => shakeDigest.digest(data, 8)).not.toThrow();
	})
})