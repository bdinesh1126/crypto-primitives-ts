/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {SymmetricAuthenticatedEncryptionService} from "../../src/symmetric/symmetric_authenticated_encryption_service";
import {SymmetricEncryptionAlgorithm} from "../../src/symmetric/symmetric_encryption_algorithm";
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";
import {IllegalStateError} from "../../src/error/illegal_state_error";

let saeService: SymmetricAuthenticatedEncryptionService;

beforeAll(() => {
	saeService = new SymmetricAuthenticatedEncryptionService(SymmetricEncryptionAlgorithm.AES256_GCM_NOPADDING());

});


describe("SymmetricAuthenticatedEncryptionService calling", () => {
	describe("genCiphertextSymmetric with", () => {
		let encryptionKey: ImmutableUint8Array;
		let plaintext: ImmutableUint8Array;
		let associatedData: string[];

		beforeEach(() => {
			encryptionKey = ImmutableUint8Array.from([1]);
			plaintext = ImmutableUint8Array.from([2]);
			associatedData = ["abc"];
		});

		test("null parameters throws NullPointerError", () => {
			saeService.genCiphertextSymmetric(null, plaintext, associatedData).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.ciphertext.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
			saeService.genCiphertextSymmetric(encryptionKey, null, associatedData).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.ciphertext.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
			saeService.genCiphertextSymmetric(encryptionKey, plaintext, null).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.ciphertext.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
		});

		test("associated data containing null elements throws IllegalArgumentError", () => {
			const associatedDataWithNull = ["abc", null];
			saeService.genCiphertextSymmetric(encryptionKey, plaintext, associatedDataWithNull).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.ciphertext.value().toString()) },
				(reason) => expect(reason).toEqual(new IllegalArgumentError("The associated data must not contain null objects."))
			);
		});
	});

	describe("getPlaintextSymmetric with", () => {
		let encryptionKey: ImmutableUint8Array;
		let nonce: ImmutableUint8Array;
		let ciphertext: ImmutableUint8Array;
		let associatedData: string[];

		beforeEach(() => {
			encryptionKey = ImmutableUint8Array.from([1]);
			nonce = ImmutableUint8Array.from([2]);
			ciphertext = ImmutableUint8Array.from([3]);
			associatedData = ["abc"];
		});

		test("null parameters throws NullPointerError", () => {
			saeService.getPlaintextSymmetric(null, ciphertext, nonce, associatedData).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
			saeService.getPlaintextSymmetric(encryptionKey, null, nonce, associatedData).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
			saeService.getPlaintextSymmetric(encryptionKey, ciphertext, null, associatedData).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
			saeService.getPlaintextSymmetric(encryptionKey, ciphertext, nonce, null).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.value().toString()) },
				(reason) => expect(reason).toEqual(new NullPointerError())
			);
		});

		test("associated data containing null elements throws IllegalArgumentError", () => {
			const associatedDataWithNull = ["abc", null];
			saeService.getPlaintextSymmetric(encryptionKey, ciphertext, nonce, associatedDataWithNull).then(
				(result) => { throw new IllegalStateError("Expected to throw but got: " + result.value().toString()) },
				(reason) => expect(reason).toEqual(new IllegalArgumentError("The associated data must not contain null objects."))
			);
		});
	});

	test("authenticatedEncryption and then authenticatedDecryption returns initial plaintext", async () => {
		const key = SecureRandomGenerator.genRandomBytes(256 / 8); // 32 bytes
		const iv = SecureRandomGenerator.genRandomBytes(12);
		const plainText = SecureRandomGenerator.genRandomBytes(44);
		const associatedData = SecureRandomGenerator.genRandomBytes(44);

		const encText = await saeService.authenticatedEncryption(key, iv, plainText, associatedData);
		const decText = await saeService.authenticatedDecryption(key, iv, encText, associatedData);

		expect(decText).toEqual(plainText);
	});
});