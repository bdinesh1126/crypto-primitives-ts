/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ImmutableUint8Array} from "../../src/immutable_uint8Array";
import getPlaintextRealValues from './get-plaintext-symmetric.json';
import {Base64} from "js-base64";
import {SymmetricService} from "../../src/symmetric/symmetric_service";
import {SecureRandomGenerator} from "../../src/generator/secure_random_generator";

let symmetricService: SymmetricService;

beforeAll(() => {
	symmetricService = new SymmetricService();
});

describe("", () => {
	describe('getPlaintextSymmetric with real values gives expected output', () => {

		interface JsonFileArgument {
			encryption_key: ImmutableUint8Array;
			ciphertext: ImmutableUint8Array;
			nonce: ImmutableUint8Array;
			associated_data: string[];
			plaintext: ImmutableUint8Array;
			description: string;
		}

		function jsonFileArgumentProvider(): JsonFileArgument[] {
			const parametersList: any[] = JSON.parse(JSON.stringify(getPlaintextRealValues));

			const args: JsonFileArgument[] = [];
			parametersList.forEach(testParameters => {
				const encryptionKey: ImmutableUint8Array = readValue(testParameters.input.encryption_key);
				const ciphertext: ImmutableUint8Array = readValue(testParameters.input.ciphertext);
				const nonce: ImmutableUint8Array = readValue(testParameters.input.nonce);
				const associatedData: string[] = testParameters.input.associated_data;

				const plaintext: ImmutableUint8Array = readValue(testParameters.output.plaintext);

				const description: string = testParameters.description;

				args.push({
					encryption_key: encryptionKey,
					ciphertext: ciphertext,
					nonce: nonce,
					associated_data: associatedData,
					plaintext: plaintext,
					description: description
				})
			});

			return args;
		}

		test.each(jsonFileArgumentProvider())("$description", async ({
																   encryption_key,
																   ciphertext,
																   nonce,
																   associated_data,
																   plaintext,
																   // @ts-ignore description is used by test definition
																   description
															   }) => {
			const result: ImmutableUint8Array = await symmetricService.getPlaintextSymmetric(encryption_key, ciphertext, nonce, associated_data);
			expect(result).toEqual(plaintext);
		});
	});

	test("genCiphertextSymmetric and then getPlaintextSymmetric returns initial plaintext", async () => {
		const key = SecureRandomGenerator.genRandomBytes(256 / 8); // 32 bytes
		const plainText = SecureRandomGenerator.genRandomBytes(44);
		const associatedData = ["a", "b", "c"];

		const encText = await symmetricService.genCiphertextSymmetric(key, plainText, associatedData);
		const decText = await symmetricService.getPlaintextSymmetric(key, encText.ciphertext, encText.nonce, associatedData);

		expect(decText).toEqual(plainText);
	});
})

function readValue(value: string): ImmutableUint8Array {
	return ImmutableUint8Array.from(Base64.toUint8Array(value));
}