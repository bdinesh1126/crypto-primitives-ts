/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {RandomService} from "../../src/math/random_service";
import {PlaintextEqualityProofService} from "../../src/zeroknowledgeproofs/plaintext_equality_proof_service";
import {GroupVector} from "../../src/group_vector";
import {ZqElement} from "../../src/math/zq_element";
import {ZqGroup} from "../../src/math/zq_group";
import {GroupTestData} from "../tools/data/group_test_data";
import {ZqGroupGenerator} from "../tools/generators/zq_group_generator";
import {GqElement} from "../../src/math/gq_element";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {GqGroup} from "../../src/math/gq_group";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {ElGamalMultiRecipientCiphertext} from "../../src/elgamal/elgamal_multi_recipient_ciphertext";
import {ElGamalGenerator} from "../tools/generators/elgamal_generator";
import {TestHashService} from "../hashing/test_hash_service";
import {HashService} from "../../src/hashing/hash_service";
import {PlaintextEqualityProof} from "../../src/zeroknowledgeproofs/plaintext_equality_proof";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import realValuesJson from './verify-plaintext-equality.json';
import {ImmutableArray} from "../../src/immutable_array";
import {ElGamalMultiRecipientMessage} from "../../src/elgamal/elgamal_multi_recipient_message";
import {ElGamalMultiRecipientPublicKey} from "../../src/elgamal/elgamal_multi_recipient_public_key";

const {computePhiPlaintextEquality} = PlaintextEqualityProofService;

const group = GroupTestData.getGroup();
const otherGroup = GroupTestData.getDifferentGqGroup(group);
const gqGroupGenerator = new GqGroupGenerator(group);
const otherGqGroupGenerator = new GqGroupGenerator(otherGroup);
const zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(group));
const otherZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup));
const elGamalGenerator = new ElGamalGenerator(group);

const randomService = new RandomService();
const plaintextEqualityProofService = new PlaintextEqualityProofService(randomService, TestHashService.create(group.q));

describe("PlaintextEqualityProofService calling", () => {

	describe("computePhiPlaintextEquality with", () => {

		let preImage: GroupVector<ZqElement, ZqGroup>;
		let firstPublicKey: GqElement;
		let secondPublicKey: GqElement;

		beforeEach(() => {
			preImage = zqGroupGenerator.genRandomZqElementVector(2);
			firstPublicKey = gqGroupGenerator.genMember();
			secondPublicKey = gqGroupGenerator.genMember();
		});

		test("valid parameters does not throw", () => {
			const image: GroupVector<GqElement, GqGroup> = computePhiPlaintextEquality(preImage, firstPublicKey,
				secondPublicKey);

			expect(image.size).toBe(3);
		});

		test("null parameters throws NullPointerError", () => {
			expect(() => computePhiPlaintextEquality(null, firstPublicKey, secondPublicKey)).toThrow(NullPointerError);
			expect(() => computePhiPlaintextEquality(preImage, null, secondPublicKey)).toThrow(NullPointerError);
			expect(() => computePhiPlaintextEquality(preImage, firstPublicKey, null)).toThrow(NullPointerError);
		});

		test("wrong size preImage throws IllegalArgumentError", () => {
			const tooShortPreImage = zqGroupGenerator.genRandomZqElementVector(1);
			expect(() => computePhiPlaintextEquality(tooShortPreImage, firstPublicKey, secondPublicKey))
				.toThrow(new IllegalArgumentError("The preImage must be of size 2."));

			const tooLongPreImage = zqGroupGenerator.genRandomZqElementVector(3);
			expect(() => computePhiPlaintextEquality(tooLongPreImage, firstPublicKey, secondPublicKey))
				.toThrow(new IllegalArgumentError("The preImage must be of size 2."));
		});

		test("public keys having different groups throws IllegalArgumentError", () => {
			const otherGroupFirstPublicKey = otherGqGroupGenerator.genMember();
			expect(() => computePhiPlaintextEquality(preImage, otherGroupFirstPublicKey, secondPublicKey))
				.toThrow(new IllegalArgumentError("The two public keys must have the same group."));

			const otherGroupSecondPublicKey = otherGqGroupGenerator.genMember();
			expect(() => computePhiPlaintextEquality(preImage, firstPublicKey, otherGroupSecondPublicKey))
				.toThrow(new IllegalArgumentError("The two public keys must have the same group."));
		});

		test("preImage and keys having different groups throws IllegalArgumentError", () => {
			const otherGroupPreImage = otherZqGroupGenerator.genRandomZqElementVector(2);

			expect(() => computePhiPlaintextEquality(otherGroupPreImage, firstPublicKey, secondPublicKey))
				.toThrow(new IllegalArgumentError("The preImage and public keys must have the same group order."));
		});

		test("specific values gives expected image", () => {
			const gqGroup = new GqGroup(ImmutableBigInteger.fromString("11"), ImmutableBigInteger.fromString("5"),
				ImmutableBigInteger.fromString("3"));
			const zqGroup = new ZqGroup(ImmutableBigInteger.fromString("5"));
			const zero = ZqElement.create(ImmutableBigInteger.fromString("0"), zqGroup);
			const three = ZqElement.create(ImmutableBigInteger.fromString("3"), zqGroup);
			const one = GqElement.fromValue(ImmutableBigInteger.fromString("1"), gqGroup);
			const four = GqElement.fromValue(ImmutableBigInteger.fromString("4"), gqGroup);
			const five = GqElement.fromValue(ImmutableBigInteger.fromString("5"), gqGroup);
			const nine = GqElement.fromValue(ImmutableBigInteger.fromString("9"), gqGroup);

			const image: GroupVector<GqElement, GqGroup> = computePhiPlaintextEquality(GroupVector.of(zero, three), four, nine);
			const expectedImage: GroupVector<GqElement, GqGroup> = GroupVector.of(one, five, four);

			expect(expectedImage.equals(image)).toBe(true);
		});

	});

	describe("genPlaintextEquality with", () => {

		let firstCiphertext: ElGamalMultiRecipientCiphertext;
		let secondCiphertext: ElGamalMultiRecipientCiphertext;
		let firstPublicKey: GqElement;
		let secondPublicKey: GqElement;
		let randomness: GroupVector<ZqElement, ZqGroup>;
		let auxiliaryInformation: string[];

		beforeEach(() => {
			const plaintext: ElGamalMultiRecipientMessage = elGamalGenerator.genRandomMessage(1);
			firstPublicKey = gqGroupGenerator.genMember();
			secondPublicKey = gqGroupGenerator.genMember();
			randomness = zqGroupGenerator.genRandomZqElementVector(2);

			firstCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(plaintext, randomness.get(0),
				new ElGamalMultiRecipientPublicKey([firstPublicKey]));
			secondCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(plaintext, randomness.get(1),
				new ElGamalMultiRecipientPublicKey([secondPublicKey]));

			auxiliaryInformation = ["asdfsdfglkm", "sdfsd0sdfksédf"];
		});

		test("valid parameters does not throw", () => {
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				randomness, auxiliaryInformation)).not.toThrow();
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				randomness, [])).not.toThrow();
		});

		test("null parameters throws NullPointerError", () => {
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(null, secondCiphertext, firstPublicKey, secondPublicKey, randomness,
				auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, null, firstPublicKey, secondPublicKey, randomness,
				auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, null, secondPublicKey, randomness,
				auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, null, randomness,
				auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				null, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				randomness, null)).toThrow(NullPointerError);
		});

		test("auxiliary information containing null throws IllegalArgumentError", () => {
			auxiliaryInformation[0] = null;

			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				randomness, auxiliaryInformation)).toThrow(new IllegalArgumentError("The auxiliary information must not contain null objects."));
		});

		test("wrong size ciphertexts throws IllegalArgumentError", () => {
			// Wrong first ciphertext.
			const wrongSizeFirstCiphertext = elGamalGenerator.genRandomCiphertext(2);

			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(wrongSizeFirstCiphertext, secondCiphertext, firstPublicKey,
				secondPublicKey, randomness, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The first ciphertext must have exactly one phi."));

			// Wrong second ciphertext.
			const wrongSizeSecondCiphertext = elGamalGenerator.genRandomCiphertext(2);

			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, wrongSizeSecondCiphertext, firstPublicKey,
				secondPublicKey, randomness, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The second ciphertext must have exactly one phi."));
		});

		test("wrong size randomness throws IllegalArgumentError", () => {
			const shortRandomness = zqGroupGenerator.genRandomZqElementVector(1);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				shortRandomness, auxiliaryInformation)).toThrow(new IllegalArgumentError("The randomness vector must have exactly two elements."));

			const longRandomness = zqGroupGenerator.genRandomZqElementVector(3);
			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				longRandomness, auxiliaryInformation)).toThrow(new IllegalArgumentError("The randomness vector must have exactly two elements."));
		});

		describe("not all inputs from same GqGroup throws IllegalArgumentError", () => {
			interface Argument {
				firstCiphertext: ElGamalMultiRecipientCiphertext;
				secondCiphertext: ElGamalMultiRecipientCiphertext;
				firstPublicKey: GqElement;
				secondPublicKey: GqElement;
			}

			function differentGroupInputsProvider(): Argument[] {
				firstCiphertext = elGamalGenerator.genRandomCiphertext(1);
				secondCiphertext = elGamalGenerator.genRandomCiphertext(1);
				firstPublicKey = gqGroupGenerator.genMember();
				secondPublicKey = gqGroupGenerator.genMember();

				const otherElGamalGenerator = new ElGamalGenerator(otherGroup);

				const otherFirstCiphertext = otherElGamalGenerator.genRandomCiphertext(1);
				const otherSecondCiphertext = otherElGamalGenerator.genRandomCiphertext(1);
				const otherFirstPublicKey = otherGqGroupGenerator.genMember();
				const otherSecondPublicKey = otherGqGroupGenerator.genMember();

				return [
					{
						firstCiphertext: otherFirstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: secondPublicKey
					},
					{
						firstCiphertext: firstCiphertext,
						secondCiphertext: otherSecondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: secondPublicKey
					},
					{
						firstCiphertext: firstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: otherFirstPublicKey,
						secondPublicKey: secondPublicKey
					},
					{
						firstCiphertext: firstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: otherSecondPublicKey
					}
				];
			}

			test.each(differentGroupInputsProvider())("throws $exception", ({firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey}) => {
				expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey,
					secondPublicKey, randomness, auxiliaryInformation))
					.toThrow(new IllegalArgumentError("The ciphertexts and public keys must all belong to the same group."));
			});

		});

		test("randomness with group of different order throws IllegalArgumentError", () => {
			const otherRandomness = otherZqGroupGenerator.genRandomZqElementVector(2);

			expect(() => plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				otherRandomness, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The randomness and ciphertexts and public keys must have the same group order."));
		});

		test("specific values gives expected proof", () => {
			const p = ImmutableBigInteger.fromString("11");
			const q = ImmutableBigInteger.fromString("5");
			const g = ImmutableBigInteger.fromString("3");
			const gqGroup = new GqGroup(p, q, g);
			const zqGroup = ZqGroup.sameOrderAs(gqGroup);

			const zqZero = ZqElement.create(0, zqGroup);
			const zqOne = ZqElement.create(1, zqGroup);
			const zqTwo = ZqElement.create(2, zqGroup);
			const zqFour = ZqElement.create(4, zqGroup);

			const gqOne = GqElement.fromValue(ImmutableBigInteger.fromString("1"), gqGroup);
			const gqThree = GqElement.fromValue(ImmutableBigInteger.fromString("3"), gqGroup);
			const gqFive = GqElement.fromValue(ImmutableBigInteger.fromString("5"), gqGroup);

			// Inputs.
			const firstCiphertext = ElGamalMultiRecipientCiphertext.create(gqThree, [gqOne]);
			const secondCiphertext = ElGamalMultiRecipientCiphertext.create(gqThree, [gqFive]);
			const firstPublicKey = GqElement.fromValue(ImmutableBigInteger.fromString("4"), gqGroup);
			const secondPublicKey = GqElement.fromValue(ImmutableBigInteger.fromString("9"), gqGroup);
			const randomness: GroupVector<ZqElement, ZqGroup> = GroupVector.of(zqTwo, zqTwo);
			const i_aux = ["aux", "info"];

			// Service creation.
			jest.mock('../../src/math/random_service');
			const mockRandomService = new RandomService();
			jest.spyOn(mockRandomService, 'genRandomVector').mockImplementation(() => GroupVector.of(zqZero, zqTwo));

			const testHashService: HashService = TestHashService.create(gqGroup.q);

			const plaintextEqualityProofService = new PlaintextEqualityProofService(mockRandomService, testHashService);

			// Expected proof.
			const e = ZqElement.create(2, zqGroup);
			const z: GroupVector<ZqElement, ZqGroup> = GroupVector.of(zqFour, zqOne);
			const expectedProof = new PlaintextEqualityProof(e, z);

			const plaintextEqualityProof = plaintextEqualityProofService
				.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey, randomness, i_aux);
			expect(expectedProof.equals(plaintextEqualityProof)).toBe(true);
		});

	});

	describe("verifyPlaintextEquality with", () => {
		let firstCiphertext: ElGamalMultiRecipientCiphertext;
		let secondCiphertext: ElGamalMultiRecipientCiphertext;
		let firstPublicKey: GqElement;
		let secondPublicKey: GqElement;
		let randomness: GroupVector<ZqElement, ZqGroup>;
		let plaintextEqualityProof: PlaintextEqualityProof;
		let auxiliaryInformation: string[];

		beforeEach(() => {
			const plaintext: ElGamalMultiRecipientMessage = elGamalGenerator.genRandomMessage(1);
			firstPublicKey = gqGroupGenerator.genMember();
			secondPublicKey = gqGroupGenerator.genMember();
			randomness = zqGroupGenerator.genRandomZqElementVector(2);

			firstCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(plaintext, randomness.get(0),
				new ElGamalMultiRecipientPublicKey([firstPublicKey]));
			secondCiphertext = ElGamalMultiRecipientCiphertext.getCiphertext(plaintext, randomness.get(1),
				new ElGamalMultiRecipientPublicKey([secondPublicKey]));

			auxiliaryInformation = ["asdfsdfglkm", "sdfsd0sdfksédf"];
			plaintextEqualityProof = plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey,
				secondPublicKey, randomness, auxiliaryInformation);
		});

		test("valid parameters returns true", () => {
			expect(plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				plaintextEqualityProof, auxiliaryInformation)).toBe(true);
		});

		test("empty auxiliary information returns true", () => {
			const plaintextEqualityProof: PlaintextEqualityProof = plaintextEqualityProofService.genPlaintextEqualityProof(
				firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey, randomness, []);

			expect(plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				plaintextEqualityProof, [])).toBe(true);
		});

		test("null parameters throws NullPointerError", () => {
			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(null, secondCiphertext, firstPublicKey, secondPublicKey,
				plaintextEqualityProof, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, null, firstPublicKey, secondPublicKey,
				plaintextEqualityProof, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, null, secondPublicKey,
				plaintextEqualityProof, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, null,
				plaintextEqualityProof, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				null, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				plaintextEqualityProof, null)).toThrow(NullPointerError);
		});

		test("auxiliary information containing null throws IllegalArgumentError", () => {
			auxiliaryInformation[0] = null;

			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				plaintextEqualityProof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The auxiliary information must not contain null objects."));
		});

		test("wrong size ciphertexts throws IllegalArgumentError", () => {
			// Wrong first ciphertext.
			const wrongSizeFirstCiphertext = elGamalGenerator.genRandomCiphertext(2);

			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(wrongSizeFirstCiphertext, secondCiphertext, firstPublicKey,
				secondPublicKey, plaintextEqualityProof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The first ciphertext must have exactly one phi."));

			// Wrong second ciphertext.
			const wrongSizeSecondCiphertext = elGamalGenerator.genRandomCiphertext(2);

			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, wrongSizeSecondCiphertext, firstPublicKey,
				secondPublicKey, plaintextEqualityProof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The second ciphertext must have exactly one phi."));
		});

		describe("not all inputs from same GqGroup throws IllegalArgumentError", () => {
			interface Argument {
				firstCiphertext: ElGamalMultiRecipientCiphertext;
				secondCiphertext: ElGamalMultiRecipientCiphertext;
				firstPublicKey: GqElement;
				secondPublicKey: GqElement;
				plaintextEqualityProof: PlaintextEqualityProof;
			}

			function differentGroupInputsProvider(): Argument[] {
				firstCiphertext = elGamalGenerator.genRandomCiphertext(1);
				secondCiphertext = elGamalGenerator.genRandomCiphertext(1);
				firstPublicKey = gqGroupGenerator.genMember();
				secondPublicKey = gqGroupGenerator.genMember();
				randomness = zqGroupGenerator.genRandomZqElementVector(2); //Call not the beforeEach function because provider is in a describe function and not in a test function
				auxiliaryInformation = ["asdfsdfglkm", "sdfsd0sdfksédf"]; //Call not the beforeEach function because provider is in a describe function and not in a test function
				plaintextEqualityProof = plaintextEqualityProofService.genPlaintextEqualityProof(firstCiphertext, secondCiphertext, firstPublicKey,
					secondPublicKey, randomness, auxiliaryInformation);

				const otherElGamalGenerator = new ElGamalGenerator(otherGroup);

				const otherFirstCiphertext = otherElGamalGenerator.genRandomCiphertext(1);
				const otherSecondCiphertext = otherElGamalGenerator.genRandomCiphertext(1);
				const otherFirstPublicKey = otherGqGroupGenerator.genMember();
				const otherSecondPublicKey = otherGqGroupGenerator.genMember();

				return [
					{
						firstCiphertext: otherFirstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: secondPublicKey,
						plaintextEqualityProof: plaintextEqualityProof
					},
					{
						firstCiphertext: firstCiphertext,
						secondCiphertext: otherSecondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: secondPublicKey,
						plaintextEqualityProof: plaintextEqualityProof
					},
					{
						firstCiphertext: firstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: otherFirstPublicKey,
						secondPublicKey: secondPublicKey,
						plaintextEqualityProof: plaintextEqualityProof
					},
					{
						firstCiphertext: firstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: otherSecondPublicKey,
						plaintextEqualityProof: plaintextEqualityProof
					}
				];
			}

			test.each(differentGroupInputsProvider())("throws $exception", ({
																				firstCiphertext,
																				secondCiphertext,
																				firstPublicKey,
																				secondPublicKey,
																				plaintextEqualityProof
																			}) => {
				expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
					plaintextEqualityProof, auxiliaryInformation))
					.toThrow(new IllegalArgumentError("The ciphertexts and public keys must all belong to the same group."));
			});
		});

		test("not the same group order throws IllegalArgumentError", () => {

			let otherPlaintextEqualityProof: PlaintextEqualityProof = new PlaintextEqualityProof(otherZqGroupGenerator.genRandomZqElementMember(),
				otherZqGroupGenerator.genRandomZqElementVector(2));

			expect(() => plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey, secondPublicKey,
				otherPlaintextEqualityProof, auxiliaryInformation))
				.toThrow(
					new IllegalArgumentError("The plaintext equality proof must have the same group order as the ciphertexts and the public keys."));
		});


		describe('with real values gives expected result', () => {

			interface JsonFileArgument {
				firstCiphertext: ElGamalMultiRecipientCiphertext;
				secondCiphertext: ElGamalMultiRecipientCiphertext;
				firstPublicKey: GqElement;
				secondPublicKey: GqElement;
				plaintextEqualityProof: PlaintextEqualityProof;
				auxiliaryInformation: string[];
				result: boolean;
				description: string;
			}

			function jsonFileArgumentProvider(): JsonFileArgument[] {

				const parametersList: any[] = JSON.parse(JSON.stringify(realValuesJson));

				const args: JsonFileArgument[] = [];
				parametersList.forEach(testParameters => {
					// Context.
					const p: ImmutableBigInteger = readValue(testParameters.context.p);
					const q: ImmutableBigInteger = readValue(testParameters.context.q);
					const g: ImmutableBigInteger = readValue(testParameters.context.g);

					const gqGroup: GqGroup = new GqGroup(p, q, g);
					const zqGroup: ZqGroup = new ZqGroup(q);

					// Parse firstCiphertext (upper_c) parameters.
					const firstGamma: GqElement = GqElement.fromValue(readValue(testParameters.input.upper_c.gamma), gqGroup)
					const firstPhi: GqElement[] = readValues(testParameters.input.upper_c.phis).map(pk => GqElement.fromValue(pk, gqGroup)).elements();
					const firstCiphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(firstGamma, firstPhi);

					// Parse secondCiphertext (upper_c_prime) parameters.
					const secondGamma: GqElement = GqElement.fromValue(readValue(testParameters.input.upper_c_prime.gamma), gqGroup)
					const secondPhi: GqElement[] = readValues(testParameters.input.upper_c_prime.phis).map(pk => GqElement.fromValue(pk, gqGroup)).elements();
					const secondCiphertext: ElGamalMultiRecipientCiphertext = ElGamalMultiRecipientCiphertext.create(secondGamma, secondPhi);

					// Parse firstPublicKey (h) parameter.
					const h: ImmutableBigInteger = readValue(testParameters.input.h);
					const firstPublicKey: GqElement = GqElement.fromValue(h, gqGroup)

					// Parse secondPublicKey (h_prime) parameter.
					const hPrime: ImmutableBigInteger = readValue(testParameters.input.h_prime);
					const secondPublicKey: GqElement = GqElement.fromValue(hPrime, gqGroup)

					// Parse plaintextEqualityProof (proof) parameters.
					const proof: any = testParameters.input.proof;
					const e: ZqElement = ZqElement.create(readValue(proof.e), zqGroup);

					const zArray: ZqElement[] = readValues(proof.z).map(element => ZqElement.create(element, zqGroup)).elements();
					const z: GroupVector<ZqElement, ZqGroup> = GroupVector.from(zArray);
					const plaintextEqualityProof: PlaintextEqualityProof = new PlaintextEqualityProof(e, z);

					// Parse auxiliaryInformation parameters (i_aux).
					const auxiliaryInformation: string[] = testParameters.input.i_aux;

					// Parse output parameters.
					const result: boolean = testParameters.output;

					args.push({
						firstCiphertext: firstCiphertext,
						secondCiphertext: secondCiphertext,
						firstPublicKey: firstPublicKey,
						secondPublicKey: secondPublicKey,
						plaintextEqualityProof: plaintextEqualityProof,
						auxiliaryInformation: auxiliaryInformation,
						result: result,
						description: testParameters.description
					});
				});

				return args;
			}

			test.each(jsonFileArgumentProvider())("$description", ({
																	   firstCiphertext,
																	   secondCiphertext,
																	   firstPublicKey,
																	   secondPublicKey,
																	   plaintextEqualityProof,
																	   auxiliaryInformation,
																	   result,
																	   // @ts-ignore description is used by test definition
																	   description
																   }) => {

				const plaintextEqualityProofService: PlaintextEqualityProofService =
					new PlaintextEqualityProofService(randomService, new HashService()); // HashService.getIns

				const actual: boolean = plaintextEqualityProofService.verifyPlaintextEquality(firstCiphertext, secondCiphertext, firstPublicKey,
					secondPublicKey, plaintextEqualityProof, auxiliaryInformation);

				expect(result === actual);
			});
		});
	});

});

function readValues(input: string[]): ImmutableArray<ImmutableBigInteger> {
	const values: ImmutableBigInteger[] = [];
	input.forEach(value => values.push(readValue(value)));
	return ImmutableArray.from(values);
}

function readValue(value: string): ImmutableBigInteger {
	return ImmutableBigInteger.fromString(value.substring(2), 16);
}