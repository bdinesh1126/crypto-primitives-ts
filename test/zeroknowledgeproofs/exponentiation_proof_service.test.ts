/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupVector} from "../../src/group_vector";
import {ZqElement} from "../../src/math/zq_element";
import {GqGroup} from "../../src/math/gq_group";
import {GqElement} from "../../src/math/gq_element";
import {RandomService} from "../../src/math/random_service";
import {GroupTestData} from "../tools/data/group_test_data";
import {GqGroupGenerator} from "../tools/gq_group_generator";
import {ZqGroupGenerator} from "../tools/generators/zq_group_generator";
import {ZqGroup} from "../../src/math/zq_group";
import {ExponentiationProofService} from "../../src/zeroknowledgeproofs/exponentiation_proof_service";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";
import {TestHashService} from "../hashing/test_hash_service";
import {HashService} from "../../src/hashing/hash_service";
import {ImmutableBigInteger} from "../../src/immutable_big_integer";
import {ExponentiationProof} from "../../src/zeroknowledgeproofs/exponentiation_proof";
import realValuesJson from './verify-exponentiation.json';
import {ImmutableArray} from "../../src/immutable_array";

const {computePhiExponentiation} = ExponentiationProofService;

const group = GroupTestData.getGroup();
const otherGroup = GroupTestData.getDifferentGqGroup(group);
const gqGroupGenerator = new GqGroupGenerator(group);
const otherGqGroupGenerator = new GqGroupGenerator(otherGroup);
const zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(group));
const otherZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup));
const randomService = new RandomService();
const hashService = TestHashService.create(group.q);
const exponentiationProofService = new ExponentiationProofService(randomService, hashService);

const MAX_NUMBER_EXPONENTIATIONS = 10;

describe("ExponentiationProofService calling", () => {

	test("constructor with any null parameters throws NullPointerError", () => {
		expect(() => new ExponentiationProofService(null, hashService)).toThrow(NullPointerError);
		expect(() => new ExponentiationProofService(randomService, null)).toThrow(NullPointerError);
	});

	describe("computePhiExponentiation with", () => {

		let preimage: ZqElement;
		let bases: GroupVector<GqElement, GqGroup>;

		beforeEach(() => {
			const n: number = randomService.nextInt(10) + 1;
			preimage = zqGroupGenerator.genRandomZqElementMember();
			bases = gqGroupGenerator.genRandomGqElementVector(n);
		});

		test("valid parameters does not throw", () => {
			for (let i = 0; i < 10; i++) {
				expect(bases.size).toEqual(computePhiExponentiation(preimage, bases).size);
			}
		});

		test("null parameters throws NullPointerError", () => {
			expect(() => computePhiExponentiation(null, bases)).toThrow(NullPointerError);
			expect(() => computePhiExponentiation(preimage, null)).toThrow(NullPointerError);
		});

		test("empty bases throws IllegalArgumentError", () => {
			const emptyBases: GroupVector<GqElement, GqGroup> = GroupVector.of();
			expect(() => computePhiExponentiation(preimage, emptyBases))
				.toThrow(new IllegalArgumentError("The vector of bases must contain at least 1 element."));
		});

		test("different group order throws IllegalArgumentError", () => {
			const otherpreimage = otherZqGroupGenerator.genRandomZqElementMember();
			expect(() => ExponentiationProofService.computePhiExponentiation(otherpreimage, bases))
				.toThrow(new IllegalArgumentError("The preimage and the bases must have the same group order."));
		});

	});

	describe("genExponentiationProof", () => {

		const auxiliaryInformation = ["aux", "1"];
		let n: number;
		let bases: GroupVector<GqElement, GqGroup>;
		let exponent: ZqElement;
		let exponentiations: GroupVector<GqElement, GqGroup>;

		beforeEach(() => {
			n = randomService.nextInt(MAX_NUMBER_EXPONENTIATIONS) + 1;
			bases = gqGroupGenerator.genRandomGqElementVector(n);
			while (bases.elements.filter(e => e.equals(e.group.identity)).length == n) {
				// if all bases are 1, we might run into corner cases that make some tests fail.
				bases = gqGroupGenerator.genRandomGqElementVector(n)
			}
			exponent = zqGroupGenerator.genRandomZqElementMember();
			exponentiations = ExponentiationProofService.computePhiExponentiation(exponent, bases);
		});

		test("with any null parameters throws NullPointerError", () => {
			expect(() => exponentiationProofService.genExponentiationProof(null, exponent, exponentiations, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => exponentiationProofService.genExponentiationProof(bases, null, exponentiations, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, null, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, null)).toThrow(NullPointerError);
		});

		test("valid parameters does not throw", () => {
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation)).not.toThrow();
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, [])).not.toThrow();
		});

		test("incorrect hash length throws IllegalArgumentError", () => {
			const badService = new ExponentiationProofService(randomService, new HashService());
			expect(() => badService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The hash service's bit length must be smaller than the bit length of q."));
		});

		test("aux information containing null throws IllegalArgumentError", () => {
			const auxiliaryInformationWithNull = ["test", null];
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformationWithNull))
				.toThrow(new IllegalArgumentError("The auxiliary information must not contain null objects."));
		});

		test("empty bases throws IllegalArgumentError", () => {
			const emptyBases = GroupVector.of<GqElement, GqGroup>();
			expect(() => exponentiationProofService.genExponentiationProof(emptyBases, exponent, exponentiations, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The bases must contain at least 1 element."));
		});

		test("bases and exponent different size throws IllegalArgumentError", () => {
			bases = GroupVector.from([...bases.elements, gqGroupGenerator.genMember()]);
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("Bases and exponentiations must have the same size."));
		});

		test("bases and exponentiations not same group throws IllegalArgumentError", () => {
			exponentiations = otherGqGroupGenerator.genRandomGqElementVector(n);
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("Bases and exponentiations must have the same group."));
		});

		test("exponent and exponentiations not same group order throws IllegalArgumentError", () => {
			exponent = otherZqGroupGenerator.genRandomZqElementMember();
			expect(() => exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The exponent and the exponentiations must have the same group order."));
		});

		test("specific values gives expected result", () => {
			const testValues = new TestValues();
			// Input.
			const bases: GroupVector<GqElement, GqGroup> = testValues.bases;
			const exponent: ZqElement = testValues.exponent;
			const exponentiations: GroupVector<GqElement, GqGroup> = testValues.exponentiations;
			const auxiliaryInformation: string[] = testValues.auxiliaryInformation;

			const proofService: ExponentiationProofService = testValues.createExponentiationProofService();
			const expected: ExponentiationProof = testValues.createExponentiationProof();

			expect(expected.equals(proofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation))).toBe(true);
		});

	});

	describe("verifyExponentiation with", () => {

		const auxiliaryInformation = ["aux", "2"];
		let n: number;
		let bases: GroupVector<GqElement, GqGroup>;
		let exponentiations: GroupVector<GqElement, GqGroup>;
		let proof: ExponentiationProof;

		beforeEach(() => {
			n = randomService.nextInt(MAX_NUMBER_EXPONENTIATIONS) + 1;
			bases = gqGroupGenerator.genRandomGqElementVector(n);
			exponentiations = gqGroupGenerator.genRandomGqElementVector(n);
			const e: ZqElement = zqGroupGenerator.genRandomZqElementMember();
			const z: ZqElement = zqGroupGenerator.genRandomZqElementMember();
			proof = new ExponentiationProof(e, z);
		});

		test("with any null parameters throws NullPointerError", () => {
			expect(() => exponentiationProofService.verifyExponentiation(null, exponentiations, proof, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => exponentiationProofService.verifyExponentiation(bases, null, proof, auxiliaryInformation))
				.toThrow(NullPointerError);
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, null, auxiliaryInformation)).toThrow(NullPointerError);
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, null)).toThrow(NullPointerError);
		});

		test("valid parameters does not throw", () => {
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformation)).not.toThrow();
		});

		test("incorrect hash length throws IllegalArgumentError", () => {
			const badService = new ExponentiationProofService(randomService, new HashService());
			expect(() => badService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The hash service's bit length must be smaller than the bit length of q."));
		});

		test("aux information containing null throws IllegalArgumentError", () => {
			const auxiliaryInformationWithNull = ["test", null];
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformationWithNull))
				.toThrow(new IllegalArgumentError("The auxiliary information must not contain null elements."));
		});

		test("empty bases throws IllegalArgumentError", () => {
			const emptyBases = GroupVector.of<GqElement, GqGroup>();
			expect(() => exponentiationProofService.verifyExponentiation(emptyBases, exponentiations, proof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The bases must contain at least 1 element."));
		});

		test("bases and exponent different size throws IllegalArgumentError", () => {
			bases = GroupVector.from([...bases.elements, gqGroupGenerator.genMember()]);
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("Bases and exponentiations must have the same size."));
		});

		test("bases and exponentiations not same group throws IllegalArgumentError", () => {
			exponentiations = otherGqGroupGenerator.genRandomGqElementVector(n);
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("Bases and exponentiations must belong to the same group."));
		});

		test("exponent and exponentiations not same group order throws IllegalArgumentError", () => {
			const otherE: ZqElement = otherZqGroupGenerator.genRandomZqElementMember();
			const otherZ: ZqElement = otherZqGroupGenerator.genRandomZqElementMember();
			const otherProof: ExponentiationProof = new ExponentiationProof(otherE, otherZ);
			expect(() => exponentiationProofService.verifyExponentiation(bases, exponentiations, otherProof, auxiliaryInformation))
				.toThrow(new IllegalArgumentError("The proof must have the same group order as the bases."));
		});

		test("Valid proof return true", () => {
			const exponent: ZqElement = zqGroupGenerator.genRandomZqElementMember();
			const exponentiations: GroupVector<GqElement, GqGroup> = ExponentiationProofService.computePhiExponentiation(exponent, bases);
			let proof: ExponentiationProof = exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, auxiliaryInformation);
			expect(exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformation)).toBe(true);
			proof = exponentiationProofService.genExponentiationProof(bases, exponent, exponentiations, []);
			expect(exponentiationProofService.verifyExponentiation(bases, exponentiations, proof, [])).toBe(true);
		});

		test("Different AuxiliaryInformation return false", () => {
			const testValues: TestValues = new TestValues();
			const bases: GroupVector<GqElement, GqGroup> = testValues.bases;
			const exponentiations: GroupVector<GqElement, GqGroup> = testValues.exponentiations;
			const auxiliaryInformation: string[] = testValues.auxiliaryInformation;
			auxiliaryInformation[0] = "random";
			const proof: ExponentiationProof = testValues.createExponentiationProof();
			const proofService: ExponentiationProofService = testValues.createExponentiationProofService();
			expect(proofService.verifyExponentiation(bases, exponentiations, proof, auxiliaryInformation)).toBe(false);
		});

		test("Invalid proof returns false", () => {
			const testValues: TestValues = new TestValues();
			const bases: GroupVector<GqElement, GqGroup> = testValues.bases;
			const exponentiations: GroupVector<GqElement, GqGroup> = testValues.exponentiations;
			const auxiliaryInformation: string[] = testValues.auxiliaryInformation;
			const proof: ExponentiationProof = testValues.createExponentiationProof();
			const e_prime: ZqElement = proof.e.add(testValues.zThree);
			const invalidProof: ExponentiationProof = new ExponentiationProof(e_prime, proof.z);
			const proofService: ExponentiationProofService = testValues.createExponentiationProofService();
			expect(proofService.verifyExponentiation(bases, exponentiations, invalidProof, auxiliaryInformation)).toBe(false);
		});

		test("Different exponentations return false", () => {
			const testValues: TestValues = new TestValues();
			const bases: GroupVector<GqElement, GqGroup> = testValues.bases;
			const exponentiations: GroupVector<GqElement, GqGroup> = testValues.exponentiations;
			const differentExponentiations: GroupVector<GqElement, GqGroup> = GroupVector.from(exponentiations.elements.map(y => y.multiply(testValues.gNine)));
			const auxiliaryInformation: string[] = testValues.auxiliaryInformation;
			const proof: ExponentiationProof = testValues.createExponentiationProof();
			const proofService: ExponentiationProofService = testValues.createExponentiationProofService();
			expect(proofService.verifyExponentiation(bases, differentExponentiations, proof, auxiliaryInformation)).toBe(false);
		});

		test("Different bases return false", () => {
			const testValues: TestValues = new TestValues();
			const bases: GroupVector<GqElement, GqGroup> = testValues.bases;
			const differentBases: GroupVector<GqElement, GqGroup> = GroupVector.from(bases.elements.map(g => g.multiply(testValues.gFive)));
			const exponentiations: GroupVector<GqElement, GqGroup> = testValues.exponentiations;
			const auxiliaryInformation: string[] = testValues.auxiliaryInformation;
			const proof: ExponentiationProof = testValues.createExponentiationProof();
			const proofService: ExponentiationProofService = testValues.createExponentiationProofService();
			expect(proofService.verifyExponentiation(differentBases, exponentiations, proof, auxiliaryInformation)).toBe(false);
		});


		describe('with real values gives expected result', () => {

			interface JsonFileArgument {
				bases: GroupVector<GqElement, GqGroup>;
				exponentiations: GroupVector<GqElement, GqGroup>;
				exponentiationProof: ExponentiationProof;
				auxiliaryInformation: string[];
				result: boolean;
				description: string;
			}

			function jsonFileArgumentProvider(): JsonFileArgument[] {

				const parametersList: any[] = JSON.parse(JSON.stringify(realValuesJson));

				const args: JsonFileArgument[] = [];
				parametersList.forEach(testParameters => {

					// Context.
					const p: ImmutableBigInteger = readValue(testParameters.context.p);
					const q: ImmutableBigInteger = readValue(testParameters.context.q);
					const g: ImmutableBigInteger = readValue(testParameters.context.g);

					const gqGroup: GqGroup = new GqGroup(p, q, g);
					const zqGroup: ZqGroup = new ZqGroup(q);

					// Parse bases parameters.
					const basesArray: GqElement[] = readValues(testParameters.input.bases).map(element => GqElement.fromValue(element, gqGroup)).elements();
					const bases: GroupVector<GqElement, GqGroup> = GroupVector.from(basesArray);

					// Parse exponentiations parameters.
					const exponentiationsArray: GqElement[] = readValues(testParameters.input.statement).map(element => GqElement.fromValue(element, gqGroup)).elements();
					const exponentiations: GroupVector<GqElement, GqGroup> = GroupVector.from(exponentiationsArray);

					// Parse decryption proof parameters.
					const proof: any = testParameters.input.proof;
					const e: ZqElement = ZqElement.create(readValue(proof.e), zqGroup);
					const z: ZqElement = ZqElement.create(readValue(proof.z), zqGroup);
					const exponentiationProof: ExponentiationProof = new ExponentiationProof(e, z);

					// Parse auxiliary information parameters.
					const auxiliaryInformation: string[] = testParameters.input.additional_information;

					// Parse output parameters.
					const result: boolean = testParameters.output.verif_result[0];

					args.push({
						bases: bases,
						exponentiations: exponentiations,
						exponentiationProof: exponentiationProof,
						auxiliaryInformation: auxiliaryInformation,
						result: result,
						description: testParameters.description
					});
				});

				return args;
			}

			test.each(jsonFileArgumentProvider())("$description", ({
																	   bases,
																	   exponentiations,
																	   exponentiationProof,
																	   auxiliaryInformation,
																	   result,
																	   // @ts-ignore description is used by test definition
																	   description
																   }) => {
				const exponentationProofService: ExponentiationProofService = new ExponentiationProofService(randomService, new HashService());

				const actual: boolean = exponentationProofService.verifyExponentiation(bases, exponentiations, exponentiationProof,
					auxiliaryInformation);

				expect(result === actual);
			});
		});

	});

});

function readValues(input: string[]): ImmutableArray<ImmutableBigInteger> {
	const values: ImmutableBigInteger[] = [];
	input.forEach(value => values.push(readValue(value)));
	return ImmutableArray.from(values);
}

function readValue(value: string): ImmutableBigInteger {
	return ImmutableBigInteger.fromString(value.substring(2), 16);
}

class TestValues {

	private readonly p = ImmutableBigInteger.fromNumber(11);
	private readonly q = ImmutableBigInteger.fromNumber(5);
	private readonly g = ImmutableBigInteger.fromNumber(3);
	private readonly gqGroup = new GqGroup(this.p, this.q, this.g);
	public readonly gThree = GqElement.fromValue(ImmutableBigInteger.fromNumber(3), this.gqGroup);
	public readonly gFour = GqElement.fromValue(ImmutableBigInteger.fromNumber(4), this.gqGroup);
	public readonly gFive = GqElement.fromValue(ImmutableBigInteger.fromNumber(5), this.gqGroup);
	public readonly gNine = GqElement.fromValue(ImmutableBigInteger.fromNumber(9), this.gqGroup);
	private readonly zqGroup = new ZqGroup(this.q);
	public readonly zTwo = ZqElement.create(ImmutableBigInteger.fromNumber(2), this.zqGroup);
	public readonly zThree = ZqElement.create(ImmutableBigInteger.fromNumber(3), this.zqGroup);

	// Input arguments:
	private readonly basesInternal = GroupVector.of<GqElement, GqGroup>(this.gFour, this.gThree);
	private readonly exponentInternal = this.zThree;
	private readonly exponentiationsInternal = GroupVector.of<GqElement, GqGroup>(this.gNine, this.gFive);
	private readonly auxiliaryInformationInternal = ["specific", "test", "values"];

	// Output:
	private readonly e = this.zTwo;
	private readonly z = this.zThree;

	get bases(): GroupVector<GqElement, GqGroup> {
		return this.basesInternal;
	}

	get exponent(): ZqElement {
		return this.exponentInternal;
	}

	get exponentiations(): GroupVector<GqElement, GqGroup> {
		return this.exponentiationsInternal;
	}

	get auxiliaryInformation(): string[] {
		return this.auxiliaryInformationInternal;
	}

	createExponentiationProofService(): ExponentiationProofService {
		const randomService: RandomService = this.getSpecificRandomService();
		const hashService: HashService = TestHashService.create(this.q);
		return new ExponentiationProofService(randomService, hashService);
	}

	createExponentiationProof(): ExponentiationProof {
		return new ExponentiationProof(this.e, this.z);
	}

	private getSpecificRandomService(): RandomService {

		return new class extends RandomService {

			private randomValues = [ImmutableBigInteger.fromNumber(2)];

			// @ts-ignore
			genRandomInteger(upperBound: ImmutableBigInteger): ImmutableBigInteger {
				return this.randomValues[0];
			}
		};
	}
}