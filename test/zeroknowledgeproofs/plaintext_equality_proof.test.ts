/*
 * Copyright 2022 Post CH Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GroupTestData} from "../tools/data/group_test_data";
import {ZqGroupGenerator} from "../tools/generators/zq_group_generator";
import {ZqGroup} from "../../src/math/zq_group";
import {ZqElement} from "../../src/math/zq_element";
import {GroupVector} from "../../src/group_vector";
import {PlaintextEqualityProof} from "../../src/zeroknowledgeproofs/plaintext_equality_proof";
import {NullPointerError} from "../../src/error/null_pointer_error";
import {IllegalArgumentError} from "../../src/error/illegal_argument_error";

const group = GroupTestData.getGroup();
const otherGroup = GroupTestData.getDifferentGqGroup(group);
const zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(group));
const otherZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGroup));

let e: ZqElement;
let z: GroupVector<ZqElement, ZqGroup>;

beforeEach(() => {
	e = zqGroupGenerator.genRandomZqElementMember();
	z = zqGroupGenerator.genRandomZqElementVector(2);
});

describe("A PlaintextEqualityProof", () => {

	test("equals works as expected", () => {
		const proof1 = new PlaintextEqualityProof(e, z);
		const z2 = zqGroupGenerator.genRandomZqElementVector(2);
		const proof2 = new PlaintextEqualityProof(e, z2);
		const e3 = zqGroupGenerator.otherElement(e);
		const proof3 = new PlaintextEqualityProof(e3, z);
		const proof4 = new PlaintextEqualityProof(e, z);

		expect(proof1.equals(null)).toBe(false);
		expect(proof1.equals(proof1)).toBe(true);
		expect(proof2.equals(proof1)).toBe(false);
		expect(proof3.equals(proof1)).toBe(false);
		expect(proof4.equals(proof1)).toBe(true);
	});


	describe("constructed with", () => {

		test("valid parameters gives valid proof", () => {
			const plaintextEqualityProof = new PlaintextEqualityProof(e, z);

			expect(plaintextEqualityProof.e.group.equals(plaintextEqualityProof.z.group)).toBe(true);
			expect(plaintextEqualityProof.z.size).toEqual(2);
		});


		test("any null parameter throws NullPointerError", () => {
			expect(() => new PlaintextEqualityProof(null, z)).toThrow(NullPointerError);
			expect(() => new PlaintextEqualityProof(e, null)).toThrow(NullPointerError);
		});

		test("z not having exactly two elements throws IllegalArgumentError", () => {
			const shortZ = zqGroupGenerator.genRandomZqElementVector(1);
			expect(() => new PlaintextEqualityProof(e, shortZ)).toThrow(new IllegalArgumentError("z must have exactly two elements."));

			const longZ = zqGroupGenerator.genRandomZqElementVector(3);
			expect(() => new PlaintextEqualityProof(e, longZ)).toThrow(new IllegalArgumentError("z must have exactly two elements."));
		});

		test("e and z from different groups throws IllegalArgumentError", () => {
			const otherGroupE = otherZqGroupGenerator.genRandomZqElementMember();

			expect(() => new PlaintextEqualityProof(otherGroupE, z)).toThrow(new IllegalArgumentError("e and z must be from the same group."));
		});

	});

});