# Crypto-Primitives TypeScript Swiss Post

---

Crypto-Primitives TypeScript  
Copyright 2022 Post CH Ltd.

The crypto-primitives Typescript library implements cryptographic algorithms of the Swiss Post Voting System developed at Post CH Ltd.

The crypto-primitives TypeScript library is E-Voting Community Program Material - please follow our [Code of Conduct](CONTRIBUTING.md) describing what you can expect from us, the Coordinated Vulnerability Disclosure Policy, and the contributing guidelines.
